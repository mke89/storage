
===================Интерфейс для поблочной работы с данными в SharedMemory===================

Для идентификации данных в SharedMemory на верхнем уровне используются дескрипторы "DataKey",
которые имеет следующее описание:

struct DataKey
{
    QString shmKey;
    int index;
    StorageError::Code errorCode;
};

Где:
    - "shmKey"	    - строковый дескриптор памяти SharedMemory, к которому должно выполняться обращение;
    - "index"	    - номер блока данных в SharedMemory с ключом "key";
    - "errorCode"   - код ошибки.


Для возврата кодов ошибок из функций могут использоваться следующие коды:
enum StorageErrorCode
{
    EC_OK,              // OK
    EC_OUT_OF_RTNGE,    // Индекс вне диапазона
    EC_DELETED,         // Данные уже удалены
};


/*!
\ Добавляет (вставляет) данные "data" в хранилище SharedMemory с идентификатором "key";
\ param[in] key - идентификатор памяти SharedMemory;
\ param[in] data - данные для сохранения;
\ return[DataKey] - Возвращается структура "DataKey", где в поле "shmKey" указывается
\                   идентификатор хранилища, в которое сохранены данные, в поле "index"
\                   указывается позиция в хранилище, по которой хранятся данные,
\                   в поле "errorCode" - указывается результат выполнения операции вставки.
*/
DataKey insert(const QString &key, const QByteArray &data);


/*!
\ Читает блоки данных из хранилища SharedMemory с идентификатором "key" по номерам, указанным в "indexes";
\ param[in] key - идентификатор памяти SharedMemory;
\ param[in] indexes - список номеров блоков данных, из которых нужно получить данные;
\ return[QList<QPair<DataKey, QByteArray>>] -   Возвращается список блоков данных и соответствующие
\                                               им дескрипторы этих данных. Проверить наличие и
\                                               корректность данных в "QByteArray" можно по его размеру и
\                                               по  полю "errorCode" структуры "DataKey".
*/
QList<QPair<DataKey, QByteArray>> select(const QString &key, const QList<int> &indexes);


/*!
\ Читает блок данных из хранилища SharedMemory с идентификатором "key" по номеру "index";
\ param[in] key - идентификатор памяти SharedMemory;
\ param[in] index - номер блока данных, по которому нужно получить данные;
\ return[QPair<DataKey, QByteArray>] -  Возвращается блок данных и соответствующий
\                                       ему дескриптор этих данных. Проверить наличие и
\                                       корректность данных в "QByteArray" можно по его размеру и
\                                       по  полю "valid" errorCode "DataKey".
*/
QPair<DataKey, QByteArray> select(const QString &key, int index);


/*!
\ Читает блок данных из хранилища SharedMemory SharedMemory по индексу данных "dataIndex";
\ param[in] dataIndex - индекс данных для извлечения данных из памяти SharedMemory;
\ return[QPair<DataKey, QByteArray>] -  Возвращается блок данных и соответствующий
\                                       ему дескриптор этих данных. Проверить наличие и
\                                       корректность данных в "QByteArray" можно по его размеру и
\                                       по  полю "valid" errorCode "DataKey".
*/
QPair<DataKey, QByteArray> select(const DataKey &dataIndex);


/*!
\ Читает все данные из хранилища SharedMemory с ключом "key";
\ param[in] key - ключ SharedMemory для чтения всех данных;
\ return[QList<QPair<DataKey, QByteArray>>] -   Возвращается блок данных и соответствующий
\                                               ему дескриптор этих данных. Проверить наличие и
\                                               корректность данных в "QByteArray" можно по его размеру и
\                                               по  полю "valid" errorCode "DataKey".
*/
QList<QPair<DataKey, QByteArray>> selectAll(const QString &key);


/*!
\ Обновляет блок данных в хранилище SharedMemory по индексу данных "dataIndex";
\ param[in] dataIndex - индекс данных для обновления в памяти SharedMemory;
\ param[in] data - данные для обновления;
\ return[StorageError::Code] - Возвращается результат выполнения операции.
*/
StorageError::Code update(const DataKey &dataIndex, const QByteArray &data);


/*!
\ Удаляет блок данных из хранилища SharedMemory по индексу данных "dataIndex";
\ param[in] dataIndex - индекс данных для удаления данных из памяти SharedMemory;
\ return[StorageError::Code] - Возвращается код выполнения операции.
*/
StorageError::Code remove(const DataKey &dataIndex);


/*!
\ Удаляет все данные из хранилища SharedMemory с ключом "key";
\ param[in] key - ключ SharedMemory для удаления всех данных;
\ return[StorageError::Code] - Возвращается код выполнения операции.
*/
StorageError::Code removeAll(const QString &key);


/*!
\ Возвращает список ключей-иднтификаторов открытых блоков SharedMemory,
\ к которым можно обращаться;
\ return[QList<QString>] - Возвращается список идентификаторов SharedMemory
*/
QList<QString> storagesKeys() const;


/*!
\ Возвращает количество ячеек в блоке SharedMemory;
\ param[in] key - ключ SharedMemory для получения количества ячеек;
\ return[QList<QString>] - Возвращается количество ячеек в блоке SharedMemory
*/
int storageChunksCount(const QString &key) const;


/*!
\ Возвращает размер одной ячейки в блоке SharedMemory;
\ param[in] key - ключ SharedMemory для получения размера ячейки;
\ return[int] - Возвращается размер одной ячейки в блоке SharedMemory
*/
int storageChunksSize(const QString &key) const;


/*!
\ Возвращает размер всего блока SharedMemory;
\ param[in] key - ключ для получения размера всего блока SharedMemory;
\ return[int] - Возвращается размер всего блока SharedMemory
*/
int storageCommonSize(const QString &key) const;


/*!
\ Возвращает номер порта (UDP), на который отправляются уведомления при
\ изменении данных в SharedMemory
\ return[int] - Возвращается номер порта для получения уведомлений
*/
int getNotificationPort() const;


/*!
\ Установка флага состояния приема приложением уведомлений от самого себя.
\ Так как все комплекты хранилища, работающие из разных приложений слушают
\ уведомления и отправляют их на один порт, то при отправке уведомления
\ приложение само также его получает. В зависимости от ситуации прием от
\ самого мебя можно как включать, так и отключать (они будут приниматься
\ на нижнем уровне, но на верхний уровень пробрасываться не будут).
\ return[void] - Данные не возвращаются
*/
void setOwnNotificationsReception(bool receive);


/*!
\ Возвращает флаг состояния приема приложением уведомлений от самого себя.
\ Детальное описание см. в описании метода "setOwnNotificationsReception()"
\ return[bool] - true - прием своих уведомлений разрешен, иначе - запрещен
*/
bool receiveOwnNotifications() const;


/*!
\ Установка флага разрешения вывода отладочных сообщений
\ из функций для работы с SharedMemory
\ return[void] - Данные не возвращаются
*/
void setErrorsDisplaying(bool display);


/*!
\ Возвращает флаг состояния вывода отладочных сообщений.
\ return[bool] - true - вывод отладочных сообщений разрешен, иначе - запрещен
*/
bool displayErrors();


/*!
\ Возвращает список дескрипторов блоков SharedMemory с информацией о них.
\ Списки формируются на основе фалов настроек при его чтении.
\ Список полей см. в описании структуры "StorageDescriptor".
\ return[QList<StorageDescriptor>] - список дескрипторов блоков SharedMemory
*/
QList<StorageDescriptor> getStoragesDescriptors();


========================Описание системы уведомлений класса "Storage"========================

В процессе модификации данных в хранилищах SharedMemory класс "Storage" генерирует уведомления
об изменении данных.
Ниже приведен список методов класса "Storage", которые модифицируют данные в SharedMemory и, следовательно,
отправляют об этом уведомления:
 -  DataKey insert(const QString &key, const QByteArray &data);
 -  StorageError::Code update(const DataKey &dataIndex, const QByteArray &data);
 -  StorageError::Code remove(const DataKey &dataIndex);
 -  StorageError::Code removeAll(const QString &key);

Эти уведомления отправляются всем другим приложениям, использующим класс "Storage",
запущенным как на одной машине с источником уведомления, так на другие машины в одной подсети с ним.
Механизм уведомлений построен на базе широковещательного UDP-сокета, порт которого задается в файле
конфигурации. Так как класс "Storage" как слушает порт уведомлений, так и отправляет данные на него, то
при отправке уведомлений другим приложениям он также получает и свои уведомления. Эту возможность
можно отключить с помощью функции "void setOwnNotificationsReception(bool receive)".
Проверить, принимает ли класс "Storage" уведомления от себя можно с помощью функции
"bool receiveOwnNotifications()". Чтобы класс "Storage" передал информацию с уведомлением в приложение,
которое использует этот класс, это приложение должно подключиться к сигналу класса "Storage"
"void eventReceived(StorageEvent event)", где "event" структура с модифицированными данными уведомления и
дополнительной информацией по уведомлению и его источнику:

struct StorageEvent
{
    EventInfo eventInfo;    - структура с информацией по уведомлению;
    QByteArray data;        - модифицированные данные;
};


Структура с информацией по уведомлению:

struct EventInfo
{
    StorageOpCode      operationCode;       -   Код операции (Вставка, обновление, удаление)
    quint64            nanoSecsSinceEpoch;  -   Время операции  в наносекундах с начала эпохи
    char               appName[...];        -   Имя приложения, выполнившего операцию с данными
    uint8_t            hostAddr[...];       -   IP-адрес хоста, на котором выполнялась операция с данными
    char               shmKey[...];         -   Имя хранилища SharedMemory, над которым выполнялась операция
    int                dataCellIndex;       -   Индекс ячейки данных в хранилище, где данные были модифицированы
    StorageError::Code errorCode;           -   Код завершения операции (код ошибки)
    qint64             pid;                 -   PID процесса, выполнившего операцию с данными
    uint16_t           payloadSize;         -   Непосредственно размер полезных данных, над которыми выполнялась операция
};


==================Описание настройки конфигурации SharedMemory для Storage===================

Конфигурация класса "Storage" осуществляется в файле "SharedMemory.conf", который располагается по пути,
принятом для конкретной системы.

Для корректной работы класса "Storage" в его файле конфигурации требуется задать следующие настройки:
 -  названия разделов SharedMemory, их размер и размеры ячеек, на которые будет разделен указанный раздел.
    Ниже приведен пример дескриптора для настройки области памяти SharedMemory:

    Размеры ячеек и блоков SharedMemory указываются в БАЙТАХ.
    --------------------------------------------------------------------------------------------------------
    [Storage.StorageDescriptor]                 # Блок, содержащий описание блока SharedMemory
    description=StorageDescriptor description   # Описание блока SharedMemory
    name=StorageDescriptor                      # Имя (ключ) блока SharedMemory, посредством которого должно
                                                # осуществляться взаимодействие с этим блоком
    rowSize=32                                  # Размер одной ячейки в блоке SharedMemory
    segmentId=1                                 # Идентификатор блока SharedMemory (пока не используется)
    size=256                                    # Общий размер блока SharedMemory
    type=fixed                                  # Тип хранилища fixed/scalable (фиксированный/изменяемый).
                                                # (На данный момент хранилище с изменяемым
                                                # размером ячеек не реализовано)
    --------------------------------------------------------------------------------------------------------

    В примере выше приведен пример настройки хранилаща SharedMemory с именем "StorageDescriptor"
    общим размером (size) 256 байт. Размер одной ячейки (rowSize) хранилища равен 32 байта.
    Количество ячеек в хранилище определяется путем деления нацело общего размера хранилища
    на размер одной ячейки:
    260 / 32 = 8.125. Взяв целуу часть от результат получим 8 ячеек.

 -  номер порта, на который будут приниматься/отправляться уведомления об операциях,
    выполняемых с хранилищами SharedMemory. Пример такой настройки приведен ниже:

    [Network]
    NotificationPort=55555

    В данном случае порт для приема/отправки уведомлений равен "55555".
















