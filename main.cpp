#include <QCoreApplication>
#include <QDebug>
#include "Settings.h"
#include "Storage.h"
#include "Test.h"
#include "StorageUnitTests.h"
#include "ChainedStoragePassageSpeedTest.h"

// TODO:
// - Сделать функцию, которая будет выводить список открытых и доступных SharedMemory
// - Добавить тест для функции Storage::storageUsedChunksCount();
// - Добавить описание функции Storage::storageUsedChunksCount() в документацию
// - Добавить в документацию описание ф-ции получения nativeId для SharedMemor

// - Добавить перегрузку для select(const DataKey &dataIndex);
// - Добавить тесты для select(const DataKey &dataIndex);
// - Добавить описание select(const DataKey &dataIndex);

#include <chrono>
#include "Utils.h"
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/shm.h>
#include "JournalSender.h"
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    /*JournalEventInfo evtInfo;
    qDebug() << evtInfo.userId;
    qDebug() << "USER: " << qgetenv("USER");
    qDebug() << "USERNAME: " << qgetenv("USERNAME");
    qDebug() << "getuid(): " << getuid();
    qDebug() << "geteuid(): " << geteuid();
    qDebug() << "getlogin(): " << getlogin();
    */

    /*
    JournalSender::instance().sendEvent("Hello");
    JournalEventInfo journalEventInfo;
    QByteArray message("Hello world!!!");
    journalEventInfo.payloadSize = message.size();
    JournalSender::instance().sendEvent(JournalEvent(journalEventInfo, message));
    */

    //JournalSender::instance().sendEvent("Hello world!");

    //qDebug() << "sizeof(StorageEventInfo): " << sizeof(StorageEventInfo);
    //qDebug() << "sizeof(JournalEventInfo): " << sizeof(JournalEventInfo);
    /*
    const QList<StorageDescriptor> &descriptors = Storage::instance().getStoragesDescriptors();
    for(const StorageDescriptor &descriptor: descriptors)
    {
        qDebug() << descriptor.toString();
    }
    */

    /*
    runChainedStoragePassageSpeedTest(2400, 101, 100);
    return 1;
    */


    runStorageUnitTests();
    return 1;


    /*
    std::chrono::time_point<std::chrono::system_clock> sysNow = std::chrono::system_clock::now();
    std::chrono::time_point<std::chrono::high_resolution_clock> hiResNow = std::chrono::high_resolution_clock::now();
    std::chrono::time_point<std::chrono::steady_clock> steadyNow = std::chrono::steady_clock::now();

    auto sysNowCount = sysNow.time_since_epoch().count();
    qDebug() << "system_clock::now: " << sysNowCount << "   " << typeid(sysNowCount).name() << "   sizeof(sysNowCount): " << sizeof(sysNowCount);

    auto hiResNowCount = hiResNow.time_since_epoch().count();
    qDebug() << "high_resolution_clock::now: " << hiResNowCount << "   " << typeid(hiResNowCount).name() << "   sizeof(hiResNowCount): " << sizeof(hiResNowCount);

    auto steadyNowCount = steadyNow.time_since_epoch().count();
    qDebug() << "steady_clock::now: " << steadyNowCount << "   " << typeid(steadyNowCount).name() << "   sizeof(steadyNowCount): " << sizeof(steadyNowCount);

    auto x = 1;
    qDebug() << "x: " << x << "   " << typeid(x).name() << "   sizeof(x): " << sizeof(x);

    qDebug() << std::chrono::steady_clock::now().time_since_epoch().count();
    qDebug() << std::chrono::steady_clock::now().time_since_epoch().count();


    std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
    auto now_ns = std::chrono::time_point_cast<std::chrono::microseconds>(now);

    auto value = now_ns.time_since_epoch();
    long duration = value.count();

    std::chrono::microseconds dur(duration);

    std::chrono::time_point<std::chrono::system_clock> dt(dur);

    if (dt != now_ns)
    {
        qDebug() << "Failure.";
    }
    else
    {
        qDebug() << "Success.";
    }

    qDebug() << now.time_since_epoch().count();
    qDebug() << dt.time_since_epoch().count();
    qDebug() << now_ns.time_since_epoch().count();
    qDebug() << duration;

    //============================================

    qDebug() << "====";
    qDebug() << std::chrono::time_point_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now()).time_since_epoch().count();
    qDebug() << std::chrono::time_point_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now()).time_since_epoch().count();
    qDebug() << QDateTime::currentDateTime().toMSecsSinceEpoch();
    qDebug() << "====";
    qDebug() << Utils::getNanoSecsSinceEpoch();
    qDebug() << Utils::getMicroSecsSinceEpoch();
    qDebug() << Utils::getMilliSecsSinceEpoch();


    //const std::time_t t_c;
    //std::tm start{};
    */

    /*
    qDebug() << "Notification port: " << Storage::instance().getNotificationPort();
    qDebug() << "sizeof(EventInfo): " << sizeof(EventInfo);
    qDebug() << "sizeof(StorageOpCode): " << sizeof(StorageOpCode);
    qDebug() << "sizeof(int): " << sizeof(int);
    */

    /*
    const QList<QString> &keys = Settings::instance().groupKeys("Storages");
    for(const QString &key: keys)
    {
        qDebug() << key;
    }
    */

    //return a.exec();
}

