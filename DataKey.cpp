#include "DataKey.h"

DataKey::DataKey():
    shmKey(QString()),
    index(-1),
    errorCode(StorageError::Code::EC_UNDEFINED)
{}

DataKey::DataKey(const DataKey &dataKey):
    shmKey(dataKey.shmKey),
    index(dataKey.index),
    errorCode(dataKey.errorCode)
{
}

DataKey::DataKey(const QString &shmKey, int index, StorageError::Code code):
    shmKey(shmKey),
    index(index),
    errorCode(code)
{}

QString DataKey::errorString() const
{
    return StorageError::toString(errorCode);
}

QString DataKey::toString() const
{
    return QString("shmKey: '%1'; index: '%2'; errorCode: '%3' - '%4'").
            arg(shmKey).arg(index).arg((int)errorCode).arg(errorString());
}

bool DataKey::isNull()
{
    return (shmKey.isEmpty()) && (index < 0) &&
           (errorCode == StorageError::Code::EC_UNDEFINED);
}

namespace StorageError
{
    QString toString(const Code &errorCode)
    {
        switch(errorCode)
        {
            case StorageError::Code::EC_OK:
            {
                return QString("OK");
            }

            case StorageError::Code::EC_INDEX_OUT_OF_RANGE:
            {
                return QString("Index out of range");
            }

            case StorageError::Code::EC_INDEX_IS_NEGATIVE:
            {
                return QString("Index is negative");
            }

            case StorageError::Code::EC_DELETED:
            {
                return QString("Already deleted");
            }

            case StorageError::Code::EC_KEY_NOT_AVAILABLE:
            {
                return QString("Key not available");
            }

            case StorageError::Code::EC_INVALID_CHUNK_DATA_SIZE:
            {
                return QString("Invalid chunk data size");
            }

            case StorageError::Code::EC_DATA_SIZE_TOO_BIG:
            {
                return QString("Data size too big");
            }

            case StorageError::Code::EC_LOCK_ERROR:
            {
                return QString("Lock error");
            }

            case StorageError::Code::EC_UNLOCK_ERROR:
            {
                return QString("Unlock error");
            }

            case StorageError::Code::EC_NO_FREE_SPACE:
            {
                return QString("No free space");
            }

            case StorageError::Code::EC_DATA_NOT_AVAILABLE_IN_INDEX:
            {
                return QString("Data not available in index");
            }

            case StorageError::Code::EC_UDP_SOCKET_NOT_INITIALISED:
            {
                return QString("UDP socket not initialised");
            }

            case StorageError::Code::EC_UDP_NOTIFY_SEND_ERROR:
            {
                return QString("UDP notify send error");
            }

            default:
                return QString("Undefined");
        }
    }
}

