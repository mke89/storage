#ifndef UTILS_H
#define UTILS_H

#include <QtGlobal>
#include <QList>
#include <QString>

class Utils
{
    public:
        struct IPv4Address
        {
            uchar byte1;
            uchar byte2;
            uchar byte3;
            uchar byte4;

            IPv4Address();
            IPv4Address(uchar b1, uchar b2, uchar b3, uchar b4);
            IPv4Address(const uint8_t *address);

            bool equal(const IPv4Address &addr) const;
            QString toString() const;
            static uint32_t bytesToUInt(const uint8_t *address);
        };

        Utils();
        static quint64 getNanoSecsSinceEpoch();
        static quint64 getMicroSecsSinceEpoch();
        static quint64 getMilliSecsSinceEpoch();
        static QList<uint32_t> getCurrentIPv4AllAddresses();
        static IPv4Address IPv4AddressFromQUint32(quint32 address);
        static bool copyFromQStringToCharArr(char *arr, int arrSize, const QString &string);
};

#endif // UTILS_H
