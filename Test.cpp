#include "Test.h"
#include <QDebug>

bool checkTestValid(TestFunc function, TestStatistics &testStat, const QString &funcName)
{
    testStat.total++;
    if(function())
    {
        testStat.passed++;
        qDebug() << "PASS test[" << testStat.total << "]   " << funcName;
        return true;
    }
    else
    {
        testStat.failed++;
        qDebug() << "- FAILED test[" << testStat.total << "]   " << funcName;
        return false;
    }
}

void printStat(TestStatistics testStat)
{
    qDebug() << "\n================";
    qDebug() << "TOTAL: " << testStat.total;
    qDebug() << "PASSED: " << testStat.passed;
    qDebug() << "FAILED: " << testStat.failed;
}


