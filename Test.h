#ifndef TEST_H
#define TEST_H

#include <QString>

typedef bool(*TestFunc)();
struct TestStatistics
{
    int total;
    int passed;
    int failed;

    TestStatistics():
        total(0),
        passed(0),
        failed(0)
    {}
};

void printStat(TestStatistics testStat);
bool checkTestValid(TestFunc function, TestStatistics &testStat, const QString &funcName);

#endif // TEST_H
