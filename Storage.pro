QT -= gui
QT += network

CONFIG += c++11 console
CONFIG -= app_bundle

include(../defineDirs.pri)
include(./Storage.pri)

DEFINES += QT_DEPRECATED_WARNINGS


SOURCES += \
        ChainedStoragePassageSpeedTest.cpp \
        StorageUnitTests.cpp \
        Test.cpp \
        main.cpp

HEADERS += \
    ChainedStoragePassageSpeedTest.h \
    StorageUnitTests.h \
    Test.h

DISTFILES += \
    StorageInfo.txt

