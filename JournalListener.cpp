#include "JournalListener.h"
#include "Settings.h"
#include <QNetworkDatagram>

QString JournalListener::confFileName = "SharedMemory";

JournalListener::JournalListener():
    journalPort(0),
    journalSocket(nullptr)
{
    journalPort = loadJournalPort();
    if(!journalPort)
    {
        return;
    }

    journalSocket = initJournalSocket(journalPort);
}

JournalListener::~JournalListener()
{
}

JournalListener& JournalListener::instance()
{
    static JournalListener instance;
    return instance;
}

quint16 JournalListener::loadJournalPort()
{
    const QList<QString> &keys = StorageSettings::Settings::instance(confFileName).
                                        groupKeys(SettingsParams::NETWORK_GROUP);
    QString journalPortString;
    bool containsJournalPort = false;

    for(const QString &key: keys)
    {
        if(key.toLower() == SettingsParams::JOURNAL_PORT.toLower())
        {
            containsJournalPort = true;
            journalPortString = key;
            break;
        }
    }

    if(!containsJournalPort)
    {
        qDebug() << "JournalListener ERROR: journal port key not found...";
        return 0;
    }

    bool ok = false;
    quint16 port = StorageSettings::Settings::instance(confFileName).value(SettingsParams::NETWORK_GROUP + "/" +
                                                                           journalPortString).toUInt(&ok);
    if(!ok)
    {
        qDebug() << "JournalListener ERROR: journal port parsing error...";
        return 0;
    }

    return port;
}

QUdpSocket* JournalListener::initJournalSocket(quint16 journalPort)
{
    if(journalPort == 0)
    {
        qDebug() << "JournalListener ERROR: incorrect journal port...";
        return nullptr;
    }

    QUdpSocket* journalSocket = new QUdpSocket();
    if(!journalSocket)
    {
        qDebug() << "JournalListener ERROR: journal socket allocation error...";
        return nullptr;
    }

    connect(journalSocket, &QUdpSocket::readyRead, this, &JournalListener::readPendingDatagrams);

    if(!journalSocket->bind(QHostAddress::Broadcast, journalPort, QAbstractSocket::ReuseAddressHint))
    {
        qDebug() << "JournalListener ERROR: journal socket binding error...";
        printUdpSocketError(journalSocket);
        return nullptr;
    }

    qDebug() << "JournalListener: journal socket initialisation OK...";
    return journalSocket;
}

void JournalListener::printUdpSocketError(const QUdpSocket *socket)
{
    QString errorDecsription = QString("Error: %1; errorString: '%2'").
            arg(socket->error()).arg(socket->errorString());
    qDebug() << errorDecsription.toLatin1().constData();
}

void JournalListener::readPendingDatagrams()
{
    while(journalSocket->hasPendingDatagrams())
    {
        qint64 pendingDatagramSize = journalSocket->pendingDatagramSize();
        const QNetworkDatagram &datagram = journalSocket->receiveDatagram(pendingDatagramSize);
        uint32_t senderIp = datagram.senderAddress().toIPv4Address();

        uint16_t datagramSize = datagram.data().size();
        uint16_t payloadSize = datagramSize - sizeof(JournalEventInfo);
        JournalEventInfo eventInfo;
        memcpy(&eventInfo, datagram.data().constData(), sizeof(JournalEventInfo));
        if(payloadSize != eventInfo.payloadSize)
        {
            qDebug() << "JournalListener ERROR: incorrect payload size";
            return;
        }

        eventInfo.setHostAddress(senderIp);
        JournalEventInfo *eventInfoPtr = (JournalEventInfo*)datagram.data().data();
        eventInfoPtr->setHostAddress(senderIp);

        emit eventReceived(datagram.data());
        emit eventReceived(JournalEvent(eventInfo, datagram.data().constData() + sizeof(JournalEventInfo)));
    }
}

quint16 JournalListener::getJournalPort() const
{
    return journalPort;
}

