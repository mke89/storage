#ifndef JOURNALSENDER_H
#define JOURNALSENDER_H

#include <QUdpSocket>
#include "SEvent.h"

class JournalSender
{
    public:
        static JournalSender& instance();
        quint16 getJournalPort() const;

        bool sendEvent(const JournalEvent &event);
        bool sendEvent(const JournalEventInfo &eventInfo, const QString &message);

        bool sendEvent(const QString &message, const QString &messageType = "SystemEvent");
        bool sendEvent(const UserIdentificator &userIdentificator, const QString &message, const QString &messageType = "SystemEvent");

    private:
        JournalSender();
        JournalSender(const JournalSender& object);
        JournalSender& operator=(const JournalSender&);
        ~JournalSender();

        quint16 loadJournalPort();
        QUdpSocket* initJournalSocket(quint16 journalPort);
        void printUdpSocketError(const QUdpSocket *socket);

        quint16 journalPort;
        QUdpSocket *journalSocket;

        static QString confFileName;
};

#endif // JOURNALSENDER_H
