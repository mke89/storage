#ifndef DATAKEY_H
#define DATAKEY_H

#include <QString>
#include <QMetaType>

namespace StorageError
{
    enum class Code: int
    {
        EC_UNDEFINED,
        EC_OK,                          // OK
        EC_INDEX_OUT_OF_RANGE,          // Индекс вне диапазона
        EC_INDEX_IS_NEGATIVE,           // Отрицательный индекс
        EC_DELETED,                     // Данные уже удалены
        EC_KEY_NOT_AVAILABLE,           // Ключ не доступен
        EC_INVALID_CHUNK_DATA_SIZE,     // Неверный размер данных
        EC_DATA_SIZE_TOO_BIG,           // Размер входных данных превышает размер хранилища
        EC_LOCK_ERROR,                  // Ошибка блокировки SharedMemory
        EC_UNLOCK_ERROR,                // Ошибка разблокировки SharedMemory
        EC_NO_FREE_SPACE,               // Нет свободного места (при вставке)
        EC_DATA_NOT_AVAILABLE_IN_INDEX, // Отсутствуют данные в этом индексе

        EC_UDP_SOCKET_NOT_INITIALISED,  // Сокет для уведомлений не инициализирован
        EC_UDP_NOTIFY_SEND_ERROR,       // Ошибка отпраики данных уведомления по UDP
        EC_UDP_JOURNAL_SEND_ERROR,      // Ошибка отпраики данных журналирования по UDP
    };

    QString toString(const Code &errorCode);
}

struct DataKey
{
    QString shmKey;
    int index;
    StorageError::Code errorCode;

    DataKey();
    DataKey(const DataKey &dataKey);
    DataKey(const QString &shmKey, int index, StorageError::Code errorCode =
                                                StorageError::Code::EC_UNDEFINED);
    QString errorString() const;
    QString toString() const;
    bool isNull();
    DataKey& operator=(const DataKey& other)
    {
        this->shmKey = other.shmKey;
        this->index = other.index;
        this->errorCode = other.errorCode;
        return *this;
    }
};

#endif // DATAKEY_H
