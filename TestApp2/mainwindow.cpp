#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QNetworkDatagram>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    udpSocket = new QUdpSocket();
    connect(udpSocket, &QUdpSocket::readyRead, this, &MainWindow::readPendingDatagrams);
    if(udpSocket->bind(QHostAddress::Broadcast, port, QAbstractSocket::ReuseAddressHint))
    {
        qDebug() << "bind OK";
    }
    else
    {
        qDebug() << "bind ERR";
        return;
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pb_sendEvent_clicked()
{
    char *msg = (char*)"01234567899876543210";
    udpSocket->writeDatagram(msg, strlen(msg), QHostAddress::Broadcast, port);
}

void MainWindow::readPendingDatagrams()
{
    ui->te_messages->append("Received message");
    while(udpSocket->hasPendingDatagrams())
    {
        udpSocket->receiveDatagram(udpSocket->pendingDatagramSize());
    }
}
