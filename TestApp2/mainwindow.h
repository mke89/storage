#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QUdpSocket>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pb_sendEvent_clicked();
    void readPendingDatagrams();

private:
    const static int port = 12345;
    Ui::MainWindow *ui;
    QUdpSocket *udpSocket;
};
#endif // MAINWINDOW_H
