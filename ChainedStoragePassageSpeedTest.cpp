#include "ChainedStoragePassageSpeedTest.h"
#include "Utils.h"
#include <QDebug>
#include <QVector>

ChainedStoragePassageSpeedTest::ChainedStoragePassageSpeedTest(uint32_t sizeKBytes, uint32_t payloadSize):
    sharedMemory(nullptr),
    sizeBytes(0),
    blocksCount(0),
    payloadSize(0),
    blockSize(0)
{
    sharedMemory = new QSharedMemory("ChainedSpeedTest");
    if(!sharedMemory)
    {
        return;
    }

    if(sharedMemory->attach())
    {
        sharedMemory->detach();
    }

    if(!sharedMemory->create(sizeKBytes * 1024))
    {
        sharedMemory = nullptr;
        return;
    }

    sizeBytes = sizeKBytes * 1024;
    blocksCount = sizeBytes / (Params::HEADER_SIZE + payloadSize);
    this->payloadSize = payloadSize;
    blockSize = Params::HEADER_SIZE + payloadSize;
}

ChainedStoragePassageSpeedTest::~ChainedStoragePassageSpeedTest()
{
    if(!sharedMemory)
    {
        sharedMemory->detach();
        delete sharedMemory;
    }
}

bool ChainedStoragePassageSpeedTest::fillMemory()
{
    if(!sharedMemory)
    {
        return false;
    }

    char *data = (char*)sharedMemory->data();
    for(uint32_t currBlock = 0; currBlock < blocksCount; currBlock++)
    {
        ShMemHeader *header = (ShMemHeader*)data;
        header->size = payloadSize;
        header->timestamp = 0;
        header->index = currBlock;
        header->flags = 0;
        header->checkSum = checkSum(data, Params::CHECKABLE_DATA_LEN);

        data += blockSize;
    }

    return true;
}

quint16 ChainedStoragePassageSpeedTest::checkSum(const char *data, uint len)
{
    return qChecksum(data, len);
}

uint32_t ChainedStoragePassageSpeedTest::passThroughTheMemory()
{
    uint32_t currentBlock = 0;
    char *data = (char*)sharedMemory->data();
    while(currentBlock <= blocksCount)
    {
        ShMemHeader *header = (ShMemHeader*)data;
        if(header->checkSum != checkSum(data, Params::CHECKABLE_DATA_LEN))
        {
            break;
        }

        currentBlock++;
        data += (Params::HEADER_SIZE + header->size);
    }

    return currentBlock;
}

uint32_t ChainedStoragePassageSpeedTest::getBlocksCount()
{
    return blocksCount;
}

uint32_t ChainedStoragePassageSpeedTest::getHeaderSize()
{
    return Params::HEADER_SIZE;
}

uint32_t ChainedStoragePassageSpeedTest::getPayloadSize()
{
    return payloadSize;
}

uint32_t ChainedStoragePassageSpeedTest::getBlockSize()
{
    return blockSize;
}

void runChainedStoragePassageSpeedTest(uint32_t storageSizeKBytes, uint32_t payloadSize, int cyclesCount)
{
    ChainedStoragePassageSpeedTest speedTest(storageSizeKBytes, payloadSize);
    if(!speedTest.fillMemory())
    {
        qDebug() << "ERROR: filling memory error";
    }

    qDebug() << "Start speed test...";

    qDebug() << "==========================";
    qDebug() << "Header size        : " << speedTest.getHeaderSize();
    qDebug() << "Payload size       : " << speedTest.getPayloadSize();
    qDebug() << "Block size         : " << speedTest.getBlockSize();
    qDebug() << "Real blocks count  : " << speedTest.getBlocksCount();

    QVector<quint64> starts(cyclesCount);
    QVector<quint64> blocks(cyclesCount);
    QVector<quint64> ends(cyclesCount);

    for(int i = 0; i < cyclesCount; i++)
    {
        quint64 startTimeNanoSecs = Utils::getNanoSecsSinceEpoch();
        starts[i] = startTimeNanoSecs;
        uint32_t passedBlocks = speedTest.passThroughTheMemory();
        blocks[i] = passedBlocks;
        quint64 endTimeNanoSecs = Utils::getNanoSecsSinceEpoch();
        ends[i] = endTimeNanoSecs;
    }

    quint64 avgNanoSecs = 0;
    quint64 nanoSecsDiff = 0;
    uint32_t avgPassedBlocks = 0;
    for(int i = 0; i < cyclesCount; i++)
    {
        nanoSecsDiff = ends[i] - starts[i];
        avgNanoSecs += nanoSecsDiff;
        avgPassedBlocks += blocks[i];
    }

    avgNanoSecs /= cyclesCount;
    avgPassedBlocks /= cyclesCount;

    quint64 microSecs = avgNanoSecs / 1000;
    quint64 milliSecs = microSecs / 1000;
    quint64 seconds = milliSecs / 1000;

    qDebug() << "==========================";
    qDebug() << "End speed test";
    qDebug() << "==========================";
    qDebug() << "Nano Seconds :" << avgNanoSecs;
    qDebug() << "Micro Seconds:" << microSecs;
    qDebug() << "Milli Seconds:" << milliSecs;
    qDebug() << "Seconds      :" << seconds;
    qDebug() << "Passed blocks count: " << avgPassedBlocks;
}



