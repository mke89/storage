#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QNetworkDatagram>
#include <QScrollBar>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    Storage::instance();
    Storage::instance().setOwnNotificationsReception(ui->cb_receiveNotifications->isChecked());
    connect(&Storage::instance(), &Storage::eventReceived, this, &MainWindow::receiveEvents);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::receiveEvents(StorageEvent event)
{
    printEventInfo(event);
}

void MainWindow::on_pb_storagesList_clicked()
{
    const QList<QString> &storages = Storage::instance().storagesKeys();
    if(storages.isEmpty())
    {
        ui->te_operationsLog->append("No storages found.");
        printDelimeter();
        return;
    }

    ui->te_operationsLog->append("Storages:");
    for(const QString &key: storages)
    {
        int storageSize = Storage::instance().storageCommonSize(key);
        int chunksSize = Storage::instance().storageChunksSize(key);
        const QString &storageInfo = QString("- %1 = %2:%3").arg(key).
                arg(storageSize).arg(chunksSize);
        ui->te_operationsLog->append(storageInfo);
    }
    printDelimeter();
}

void MainWindow::on_pb_clean_clicked()
{
    ui->te_operationsLog->clear();
}

void MainWindow::on_pb_checkStorageKey_clicked()
{
    const QString &key = ui->le_storageKey->text();
    if(!storageKeySpecified(key))
    {
        return;
    }

    if(!Storage::instance().storagesKeys().contains(key))
    {
        const QString &storageInfo = QString("Key '%1' not found.").arg(key);
        ui->te_operationsLog->append(storageInfo);
        printDelimeter();
        return;
    }

    printEStorageInfo(key);
}

void MainWindow::on_pb_insertData_clicked()
{
    const QString &key = ui->le_storageKey->text();
    if(!storageKeySpecified(key))
    {
        return;
    }

    const QString &insertText = ui->le_insertData->text();
    if(!dataSpecified(insertText))
    {
        return;
    }

    if(!Storage::instance().storagesKeys().contains(key))
    {
        const QString &storageInfo = QString("Key '%1' not found.").arg(key);
        ui->te_operationsLog->append(storageInfo);
        printDelimeter();
        return;
    }

    const DataKey &dataKey = Storage::instance().insert(key, insertText.toLatin1());

    ui->te_operationsLog->append("INSERT result:");
    ui->te_operationsLog->append(QString("Storage key: '%1'").arg(dataKey.shmKey));
    ui->te_operationsLog->append(QString("Chunk index: '%1'").arg(dataKey.index));
    ui->te_operationsLog->append(QString("Error code: '%1'").arg(dataKey.errorString()));
    printDelimeter();
}

void MainWindow::on_pb_selectData_clicked()
{
    const QString &key = ui->le_storageKey->text();
    if(!storageKeySpecified(key))
    {
        return;
    }

    const QString &indexText = ui->le_selectDataIndex->text();
    if(!indexSpecified(indexText))
    {
        return;
    }

    int index = convertTextToIndex(indexText);
    if(!indexValid(index))
    {
        return;
    }

    const QPair<DataKey, QByteArray> &result = Storage::instance().select(key, index);
    ui->te_operationsLog->append("SELECT result:");
    ui->te_operationsLog->append(QString("Storage key: '%1'").arg(result.first.shmKey));
    ui->te_operationsLog->append(QString("Chunk index: '%1'").arg(result.first.index));
    ui->te_operationsLog->append(QString("Error code: '%1'").arg(result.first.errorString()));
    ui->te_operationsLog->append(QString("Data: '%1'").arg(QString(result.second)));
    printDelimeter();
}

void MainWindow::on_pb_updateData_clicked()
{
    const QString &key = ui->le_storageKey->text();
    if(!storageKeySpecified(key))
    {
        return;
    }

    const QString &indexText = ui->le_updateDataIndex->text();
    if(!indexSpecified(indexText))
    {
        return;
    }

    const QString &updateText = ui->le_updateData->text();
    if(!dataSpecified(updateText))
    {
        return;
    }

    int index = convertTextToIndex(indexText);
    if(!indexValid(index))
    {
        return;
    }

    StorageError::Code errorCode = Storage::instance().update(DataKey(key, index),
                                                                   updateText.toLatin1());
    ui->te_operationsLog->append("UPDATE result:");
    ui->te_operationsLog->append(QString("Storage key: '%1'").arg(key));
    ui->te_operationsLog->append(QString("Chunk index: '%1'").arg(index));
    ui->te_operationsLog->append(QString("Error code: '%1'").arg(StorageError::toString(errorCode)));
    ui->te_operationsLog->append(QString("Data: '%1'").arg(updateText));
    printDelimeter();
}

void MainWindow::on_pb_removeData_clicked()
{
    const QString &key = ui->le_storageKey->text();
    if(!storageKeySpecified(key))
    {
        return;
    }

    const QString &indexText = ui->le_removeDataIndex->text();
    if(!indexSpecified(indexText))
    {
        return;
    }

    int index = convertTextToIndex(indexText);
    if(!indexValid(index))
    {
        return;
    }

    StorageError::Code errorCode = Storage::instance().remove(DataKey(key, index));
    ui->te_operationsLog->append("REMOVE result:");
    ui->te_operationsLog->append(QString("Storage key: '%1'").arg(key));
    ui->te_operationsLog->append(QString("Chunk index: '%1'").arg(index));
    ui->te_operationsLog->append(QString("Error code: '%1'").arg(StorageError::toString(errorCode)));
    printDelimeter();
}

void MainWindow::on_cb_receiveNotifications_stateChanged(int arg1)
{
    if(arg1 == Qt::Unchecked)
    {
        Storage::instance().setOwnNotificationsReception(false);
        return;
    }

    if(arg1 == Qt::Checked)
    {
        Storage::instance().setOwnNotificationsReception(true);
        return;
    }
}

bool MainWindow::storageKeySpecified(const QString &key)
{
    if(key.isEmpty())
    {
        ui->te_operationsLog->append("Storage key not specified.");
        printDelimeter();
        return false;
    }

    return true;
}

bool MainWindow::indexSpecified(const QString &indexText)
{
    if(indexText.isEmpty())
    {
        ui->te_operationsLog->append("Index not specified.");
        printDelimeter();
        return false;
    }

    return true;
}

bool MainWindow::dataSpecified(const QString &dataText)
{
    if(dataText.isEmpty())
    {
        ui->te_operationsLog->append("Data not specified.");
        printDelimeter();
        return false;
    }

    return true;
}

bool MainWindow::indexValid(int index)
{
    if(index < 0)
    {
        ui->te_operationsLog->append("Invalid index.");
        printDelimeter();
        return false;
    }

    return true;
}

int MainWindow::convertTextToIndex(const QString &indexText)
{
    bool ok;
    int index = indexText.toInt(&ok);
    if(!ok)
    {
        return -1;
    }

    return index;
}

void MainWindow::printDelimeter()
{
    ui->te_operationsLog->append("=======================================");
    QScrollBar *sb = ui->te_operationsLog->verticalScrollBar();
    sb->setValue(sb->maximum());
}

void MainWindow::printEventInfo(const StorageEvent &event)
{
    ui->te_operationsLog->append("DATA RECEIVED:");
    ui->te_operationsLog->append(QString());

    const QString &opString = QString("Operation: '%1'").
                    arg(event.event.opCodeString());
    ui->te_operationsLog->append(opString);

    const QString &nanoSecsString = QString("NanoSecs since epoch: '%1'").
                    arg(event.event.namoSecsSinceEpoch);
    ui->te_operationsLog->append(nanoSecsString);

    const QString &appNameString = QString("Application name: '%1'").
                    arg(event.event.appName);
    ui->te_operationsLog->append(appNameString);

    const QString &hostAddrString = QString("Sender IP: '%1'").
                    arg(Utils::IPv4Address(event.event.hostAddr).toString());
    ui->te_operationsLog->append(hostAddrString);

    const QString &storageKeyString = QString("Storage key: '%1'").
                    arg(event.event.shmKey);
    ui->te_operationsLog->append(storageKeyString);

    const QString &recordIndexString = QString("Record index: '%1'").
                    arg(event.event.index);
    ui->te_operationsLog->append(recordIndexString);

    const QString &errStateString = QString("Error state: '%1'").
                    arg(StorageError::toString(event.event.errorCode));
    ui->te_operationsLog->append(errStateString);

    const QString &processPidString = QString("Process PID: '%1'").
                    arg(event.event.pid);
    ui->te_operationsLog->append(processPidString);

    const QString &payloadSizeString = QString("Payload size: '%1'").
                    arg(event.event.payloadSize);
    ui->te_operationsLog->append(payloadSizeString);

    ui->te_operationsLog->append(QString());
    const QString &dataString = QString("Data: '%1'").
                    arg(QString(event.data));
    ui->te_operationsLog->append(dataString);
    printDelimeter();
}

void MainWindow::printEStorageInfo(const QString &storageKey)
{
    QString storageInfo = QString("Storage key: '%1'").arg(storageKey);
    ui->te_operationsLog->append(storageInfo);

    int storageSize = Storage::instance().storageCommonSize(storageKey);
    storageInfo = QString("Storage size: %1").arg(storageSize);
    ui->te_operationsLog->append(storageInfo);

    int chunksCount = Storage::instance().storageChunksCount(storageKey);
    storageInfo = QString("Chunks count: %1").arg(chunksCount);
    ui->te_operationsLog->append(storageInfo);

    int chunkSize = Storage::instance().storageChunksSize(storageKey);
    storageInfo = QString("Chunks size: %1").arg(chunkSize);
    ui->te_operationsLog->append(storageInfo);

    printDelimeter();
}



