/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QFormLayout *formLayout;
    QLabel *label;
    QHBoxLayout *horizontalLayout;
    QLineEdit *le_storageKey;
    QPushButton *pb_checkStorageKey;
    QSpacerItem *horizontalSpacer;
    QPushButton *pb_storagesList;
    QLabel *label_2;
    QHBoxLayout *horizontalLayout_2;
    QLineEdit *le_insertData;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *pb_insertData;
    QLabel *label_3;
    QHBoxLayout *horizontalLayout_3;
    QLineEdit *le_selectDataIndex;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *pb_selectData;
    QLabel *label_4;
    QHBoxLayout *horizontalLayout_4;
    QLineEdit *le_updateData;
    QLabel *label_5;
    QLineEdit *le_updateDataIndex;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *pb_updateData;
    QLabel *label_6;
    QHBoxLayout *horizontalLayout_5;
    QLineEdit *le_removeDataIndex;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *pb_removeData;
    QTextEdit *te_operationsLog;
    QHBoxLayout *horizontalLayout_6;
    QCheckBox *cb_receiveNotifications;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *pb_clean;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(631, 422);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        formLayout = new QFormLayout();
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label = new QLabel(centralwidget);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        le_storageKey = new QLineEdit(centralwidget);
        le_storageKey->setObjectName(QStringLiteral("le_storageKey"));
        le_storageKey->setEnabled(true);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(le_storageKey->sizePolicy().hasHeightForWidth());
        le_storageKey->setSizePolicy(sizePolicy);
        le_storageKey->setMinimumSize(QSize(200, 0));
        le_storageKey->setMaximumSize(QSize(200, 16777215));

        horizontalLayout->addWidget(le_storageKey);

        pb_checkStorageKey = new QPushButton(centralwidget);
        pb_checkStorageKey->setObjectName(QStringLiteral("pb_checkStorageKey"));

        horizontalLayout->addWidget(pb_checkStorageKey);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pb_storagesList = new QPushButton(centralwidget);
        pb_storagesList->setObjectName(QStringLiteral("pb_storagesList"));

        horizontalLayout->addWidget(pb_storagesList);


        formLayout->setLayout(0, QFormLayout::FieldRole, horizontalLayout);

        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        le_insertData = new QLineEdit(centralwidget);
        le_insertData->setObjectName(QStringLiteral("le_insertData"));
        QSizePolicy sizePolicy1(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(le_insertData->sizePolicy().hasHeightForWidth());
        le_insertData->setSizePolicy(sizePolicy1);
        le_insertData->setMinimumSize(QSize(200, 0));
        le_insertData->setMaximumSize(QSize(200, 16777215));

        horizontalLayout_2->addWidget(le_insertData);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        pb_insertData = new QPushButton(centralwidget);
        pb_insertData->setObjectName(QStringLiteral("pb_insertData"));

        horizontalLayout_2->addWidget(pb_insertData);


        formLayout->setLayout(1, QFormLayout::FieldRole, horizontalLayout_2);

        label_3 = new QLabel(centralwidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_3);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        le_selectDataIndex = new QLineEdit(centralwidget);
        le_selectDataIndex->setObjectName(QStringLiteral("le_selectDataIndex"));
        sizePolicy1.setHeightForWidth(le_selectDataIndex->sizePolicy().hasHeightForWidth());
        le_selectDataIndex->setSizePolicy(sizePolicy1);
        le_selectDataIndex->setMinimumSize(QSize(200, 0));
        le_selectDataIndex->setMaximumSize(QSize(200, 16777215));

        horizontalLayout_3->addWidget(le_selectDataIndex);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);

        pb_selectData = new QPushButton(centralwidget);
        pb_selectData->setObjectName(QStringLiteral("pb_selectData"));

        horizontalLayout_3->addWidget(pb_selectData);


        formLayout->setLayout(2, QFormLayout::FieldRole, horizontalLayout_3);

        label_4 = new QLabel(centralwidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_4);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        le_updateData = new QLineEdit(centralwidget);
        le_updateData->setObjectName(QStringLiteral("le_updateData"));
        sizePolicy1.setHeightForWidth(le_updateData->sizePolicy().hasHeightForWidth());
        le_updateData->setSizePolicy(sizePolicy1);
        le_updateData->setMinimumSize(QSize(200, 0));
        le_updateData->setMaximumSize(QSize(200, 16777215));

        horizontalLayout_4->addWidget(le_updateData);

        label_5 = new QLabel(centralwidget);
        label_5->setObjectName(QStringLiteral("label_5"));

        horizontalLayout_4->addWidget(label_5);

        le_updateDataIndex = new QLineEdit(centralwidget);
        le_updateDataIndex->setObjectName(QStringLiteral("le_updateDataIndex"));
        le_updateDataIndex->setMinimumSize(QSize(200, 0));

        horizontalLayout_4->addWidget(le_updateDataIndex);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_4);

        pb_updateData = new QPushButton(centralwidget);
        pb_updateData->setObjectName(QStringLiteral("pb_updateData"));

        horizontalLayout_4->addWidget(pb_updateData);


        formLayout->setLayout(3, QFormLayout::FieldRole, horizontalLayout_4);

        label_6 = new QLabel(centralwidget);
        label_6->setObjectName(QStringLiteral("label_6"));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_6);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        le_removeDataIndex = new QLineEdit(centralwidget);
        le_removeDataIndex->setObjectName(QStringLiteral("le_removeDataIndex"));
        sizePolicy1.setHeightForWidth(le_removeDataIndex->sizePolicy().hasHeightForWidth());
        le_removeDataIndex->setSizePolicy(sizePolicy1);
        le_removeDataIndex->setMinimumSize(QSize(200, 0));
        le_removeDataIndex->setMaximumSize(QSize(200, 16777215));

        horizontalLayout_5->addWidget(le_removeDataIndex);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_5);

        pb_removeData = new QPushButton(centralwidget);
        pb_removeData->setObjectName(QStringLiteral("pb_removeData"));

        horizontalLayout_5->addWidget(pb_removeData);


        formLayout->setLayout(4, QFormLayout::FieldRole, horizontalLayout_5);


        verticalLayout->addLayout(formLayout);

        te_operationsLog = new QTextEdit(centralwidget);
        te_operationsLog->setObjectName(QStringLiteral("te_operationsLog"));

        verticalLayout->addWidget(te_operationsLog);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        cb_receiveNotifications = new QCheckBox(centralwidget);
        cb_receiveNotifications->setObjectName(QStringLiteral("cb_receiveNotifications"));

        horizontalLayout_6->addWidget(cb_receiveNotifications);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_6);

        pb_clean = new QPushButton(centralwidget);
        pb_clean->setObjectName(QStringLiteral("pb_clean"));

        horizontalLayout_6->addWidget(pb_clean);


        verticalLayout->addLayout(horizontalLayout_6);

        MainWindow->setCentralWidget(centralwidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "TestApp1", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "Storage:", Q_NULLPTR));
        le_storageKey->setText(QApplication::translate("MainWindow", "Storage1", Q_NULLPTR));
        pb_checkStorageKey->setText(QApplication::translate("MainWindow", "Check", Q_NULLPTR));
        pb_storagesList->setText(QApplication::translate("MainWindow", "Storages", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainWindow", "Data:", Q_NULLPTR));
        le_insertData->setText(QApplication::translate("MainWindow", "aaa", Q_NULLPTR));
        pb_insertData->setText(QApplication::translate("MainWindow", "INSERT", Q_NULLPTR));
        label_3->setText(QApplication::translate("MainWindow", "Index:", Q_NULLPTR));
        le_selectDataIndex->setText(QApplication::translate("MainWindow", "0", Q_NULLPTR));
        pb_selectData->setText(QApplication::translate("MainWindow", "SELECT", Q_NULLPTR));
        label_4->setText(QApplication::translate("MainWindow", "Data:", Q_NULLPTR));
        le_updateData->setText(QApplication::translate("MainWindow", "111", Q_NULLPTR));
        label_5->setText(QApplication::translate("MainWindow", "Index:", Q_NULLPTR));
        le_updateDataIndex->setText(QApplication::translate("MainWindow", "0", Q_NULLPTR));
        pb_updateData->setText(QApplication::translate("MainWindow", "UPDATE", Q_NULLPTR));
        label_6->setText(QApplication::translate("MainWindow", "Index:", Q_NULLPTR));
        le_removeDataIndex->setText(QApplication::translate("MainWindow", "0", Q_NULLPTR));
        pb_removeData->setText(QApplication::translate("MainWindow", "REMOVE", Q_NULLPTR));
        cb_receiveNotifications->setText(QApplication::translate("MainWindow", "Receive own notifications", Q_NULLPTR));
        pb_clean->setText(QApplication::translate("MainWindow", "Clean", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
