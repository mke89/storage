#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QUdpSocket>
#include "Storage.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE



class MainWindow : public QMainWindow
{
    Q_OBJECT

    public:
        MainWindow(QWidget *parent = nullptr);
        ~MainWindow();

    private:
        void printDelimeter();
        bool storageKeySpecified(const QString &key);
        bool indexSpecified(const QString &indexText);
        bool dataSpecified(const QString &dataText);
        bool indexValid(int index);
        int convertTextToIndex(const QString &indexText);
        void printEventInfo(const StorageEvent &event);
        void printEStorageInfo(const QString &storageKey);

    private slots:
        void receiveEvents(StorageEvent event);
        void on_pb_storagesList_clicked();
        void on_pb_clean_clicked();
        void on_pb_checkStorageKey_clicked();
        void on_pb_insertData_clicked();
        void on_pb_selectData_clicked();
        void on_pb_updateData_clicked();
        void on_pb_removeData_clicked();
        void on_cb_receiveNotifications_stateChanged(int arg1);

private:
        Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
