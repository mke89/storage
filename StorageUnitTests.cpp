#include "StorageUnitTests.h"
#include "Test.h"
#include "Storage.h"
#include "Settings.h"

namespace TestsParams
{
    const QString StorageDescriptor  = "StorageDescriptor";
    const QString InvalidKey  = "InvalidKey";
    const char InsertData[] = "0123456789";
    const char InsertDataTooBig[] = "0123456789012345678901234567890123456789";
    const char UpdateData[] = "abcdefghijkl";

    const int STORAGE_1_CAPACITY_BYTES  = 256;
    const int STORAGE_1_CHUNKS_SIZE     = 32;
}

bool StorageTest_getNotificationPort_valid();
bool StorageTest_storagesKeys_valid();
bool StorageTest_storageCommonSize_valid();
bool StorageTest_storageChunksSize_valid();
bool StorageTest_storageChunksCount_valid();
bool StorageTest_insert_valid();
bool StorageTest_insertInvalidKey_valid();
bool StorageTest_insertDataSizeTooBig_valid();
bool StorageTest_selectIndex_valid();
bool StorageTest_selectInvalidKey_valid();
bool StorageTest_selectNegativeIndex_valid();
bool StorageTest_selectOutOfBoundIndex_valid();
bool StorageTest_selectIndexesList_valid();
bool StorageTest_selectNoData_valid();
bool StorageTest_selectByDataKeyIndex_valid();
bool StorageTest_selectByDataKeyInvalidKey_valid();
bool StorageTest_selectByDataKeyNegativeIndex_valid();
bool StorageTest_selectByDataKeyOutOfBoundIndex_valid();
bool StorageTest_selectByDataKeyNoData_valid();
bool StorageTest_selectAll_valid();
bool StorageTest_selectAllInvalidKey_valid();
bool StorageTest_update_valid();
bool StorageTest_updateInvalidKey_valid();
bool StorageTest_updateNegativeIndex_valid();
bool StorageTest_updateOutOfBoundIndex_valid();
bool StorageTest_updateDataSizeTooBig_valid();
bool StorageTest_remove_valid();
bool StorageTest_removeInvalidKey_valid();
bool StorageTest_removeNegativeIndex_valid();
bool StorageTest_removeOutOfBoundIndex_valid();
bool StorageTest_removeAll_valid();
bool StorageTest_removeAllInvalidKey_valid();


void runStorageUnitTests()
{
    Storage::instance().setErrorsDisplaying(false);
    TestStatistics testStat;

    checkTestValid(StorageTest_getNotificationPort_valid, testStat, "StorageTest_getNotificationPort_valid");
    checkTestValid(StorageTest_storagesKeys_valid, testStat, "StorageTest_storagesKeys_valid");
    checkTestValid(StorageTest_storageCommonSize_valid, testStat, "StorageTest_storageCommonSize_valid");
    checkTestValid(StorageTest_storageChunksSize_valid, testStat, "StorageTest_storageChunksSize_valid");
    checkTestValid(StorageTest_storageChunksCount_valid, testStat, "StorageTest_storageChunksCount_valid");
    checkTestValid(StorageTest_insert_valid, testStat, "StorageTest_insert_valid");
    checkTestValid(StorageTest_insertInvalidKey_valid, testStat, "StorageTest_insertInvalidKey_valid");
    checkTestValid(StorageTest_insertDataSizeTooBig_valid, testStat, "StorageTest_insertDataSizeTooBig_valid");
    checkTestValid(StorageTest_selectIndex_valid, testStat, "StorageTest_selectIndex_valid");
    checkTestValid(StorageTest_selectInvalidKey_valid, testStat, "StorageTest_selectInvalidKey_valid");
    checkTestValid(StorageTest_selectNegativeIndex_valid, testStat, "StorageTest_selectNegativeIndex_valid");
    checkTestValid(StorageTest_selectOutOfBoundIndex_valid, testStat, "StorageTest_selectOutOfBoundIndex_valid");
    checkTestValid(StorageTest_selectIndexesList_valid, testStat, "StorageTest_selectIndexesList_valid");
    checkTestValid(StorageTest_selectNoData_valid, testStat, "StorageTest_selectNoData_valid");
    checkTestValid(StorageTest_selectByDataKeyIndex_valid, testStat, "StorageTest_selectByDataKeyIndex_valid");
    checkTestValid(StorageTest_selectByDataKeyInvalidKey_valid, testStat, "StorageTest_selectByDataKeyInvalidKey_valid");
    checkTestValid(StorageTest_selectByDataKeyNegativeIndex_valid, testStat, "StorageTest_selectByDataKeyNegativeIndex_valid");
    checkTestValid(StorageTest_selectByDataKeyOutOfBoundIndex_valid, testStat, "StorageTest_selectByDataKeyOutOfBoundIndex_valid");
    checkTestValid(StorageTest_selectByDataKeyNoData_valid, testStat, "StorageTest_selectByDataKeyNoData_valid");
    checkTestValid(StorageTest_selectAll_valid, testStat, "StorageTest_selectAll_valid");
    checkTestValid(StorageTest_selectAllInvalidKey_valid, testStat, "StorageTest_selectAllInvalidKey_valid");
    checkTestValid(StorageTest_update_valid, testStat, "StorageTest_update_valid");
    checkTestValid(StorageTest_updateInvalidKey_valid, testStat, "StorageTest_updateInvalidKey_valid");
    checkTestValid(StorageTest_updateNegativeIndex_valid, testStat, "StorageTest_updateNegativeIndex_valid");
    checkTestValid(StorageTest_updateOutOfBoundIndex_valid, testStat, "StorageTest_updateOutOfBoundIndex_valid");
    checkTestValid(StorageTest_updateDataSizeTooBig_valid, testStat, "StorageTest_updateDataSizeTooBig_valid");
    checkTestValid(StorageTest_remove_valid, testStat, "StorageTest_remove_valid");
    checkTestValid(StorageTest_removeInvalidKey_valid, testStat, "StorageTest_removeInvalidKey_valid");
    checkTestValid(StorageTest_removeNegativeIndex_valid, testStat, "StorageTest_removeNegativeIndex_valid");
    checkTestValid(StorageTest_removeOutOfBoundIndex_valid, testStat, "StorageTest_removeOutOfBoundIndex_valid");
    checkTestValid(StorageTest_removeAll_valid, testStat, "StorageTest_removeAll_valid");
    checkTestValid(StorageTest_removeAllInvalidKey_valid, testStat, "StorageTest_removeAllInvalidKey_valid");

    printStat(testStat);
}

bool StorageTest_removeAllInvalidKey_valid()
{
    QString storageKey = TestsParams::InvalidKey;
    StorageError::Code errorCode = Storage::instance().removeAll(storageKey);
    if(errorCode != StorageError::Code::EC_KEY_NOT_AVAILABLE)
    {
        return false;
    }

    return true;
}

bool StorageTest_removeAll_valid()
{
    QString storageKey = TestsParams::StorageDescriptor;
    Storage::instance().removeAll(storageKey);

    int chunksCount = Storage::instance().storageChunksCount(storageKey);
    for(int recordNum = 0; recordNum < chunksCount; recordNum++)
    {
        const DataKey &dataKey = Storage::instance().insert(storageKey, QByteArray(TestsParams::InsertData));
        if((dataKey.shmKey != storageKey) ||
           (dataKey.index != recordNum) ||
           (dataKey.errorCode != StorageError::Code::EC_OK))
        {
            return false;
        }
    }

    const QList<QPair<DataKey, QByteArray>> &selectionBefore = Storage::instance().selectAll(storageKey);
    if(selectionBefore.size() <= 1)
    {
        return false;
    }

    StorageError::Code errorCode = Storage::instance().removeAll(storageKey);
    if(errorCode != StorageError::Code::EC_OK)
    {
        return false;
    }

    const QList<QPair<DataKey, QByteArray>> &selectionAfter = Storage::instance().selectAll(storageKey);
    if(selectionAfter.size() != 0)
    {
        return false;
    }

    return true;
}

bool StorageTest_selectNoData_valid()
{
    QString storageKey = TestsParams::StorageDescriptor;
    int recordIndex = 0;
    Storage::instance().removeAll(storageKey);

    const QPair<DataKey, QByteArray> &selection = Storage::instance().select(storageKey, recordIndex);
    if((selection.first.shmKey != storageKey) ||
       (selection.first.index != recordIndex) ||
       (selection.first.errorCode != StorageError::Code::EC_DATA_NOT_AVAILABLE_IN_INDEX))
    {
        return false;
    }


    return true;
}

bool StorageTest_selectByDataKeyNoData_valid()
{
    QString storageKey = TestsParams::StorageDescriptor;
    int recordIndex = 0;
    Storage::instance().removeAll(storageKey);

    const DataKey &dataKey = DataKey(storageKey, recordIndex, StorageError::Code::EC_UNDEFINED);
    const QPair<DataKey, QByteArray> &selection = Storage::instance().select(dataKey);
    if((selection.first.shmKey != storageKey) ||
       (selection.first.index != recordIndex) ||
       (selection.first.errorCode != StorageError::Code::EC_DATA_NOT_AVAILABLE_IN_INDEX))
    {
        return false;
    }


    return true;
}

bool StorageTest_removeOutOfBoundIndex_valid()
{
    QString storageKey = TestsParams::StorageDescriptor;
    int firstInvalidIndex = Storage::instance().storageChunksCount(storageKey);
    DataKey dataKey(storageKey, firstInvalidIndex);

    StorageError::Code errorCode = Storage::instance().remove(dataKey);
    if(errorCode != StorageError::Code::EC_INDEX_OUT_OF_RANGE)
    {
        return false;
    }

    return true;
}

bool StorageTest_removeNegativeIndex_valid()
{
    QString storageKey = TestsParams::StorageDescriptor;
    int updateIndex = -5;
    DataKey dataKey(storageKey, updateIndex);

    StorageError::Code errorCode = Storage::instance().remove(dataKey);
    if(errorCode != StorageError::Code::EC_INDEX_IS_NEGATIVE)
    {
        return false;
    }

    return true;
}

bool StorageTest_removeInvalidKey_valid()
{
    QString storageKey = TestsParams::InvalidKey;
    int updateIndex = 0;
    DataKey dataKey(storageKey, updateIndex);

    StorageError::Code errorCode = Storage::instance().remove(dataKey);
    if(errorCode != StorageError::Code::EC_KEY_NOT_AVAILABLE)
    {
        return false;
    }

    return true;
}

bool StorageTest_remove_valid()
{
    QString storageKey = TestsParams::StorageDescriptor;
    int updateIndex = 0;
    DataKey dataKey(storageKey, updateIndex);

    Storage::instance().removeAll(storageKey);
    Storage::instance().insert(storageKey, QByteArray(TestsParams::InsertData));

    StorageError::Code errorCode = Storage::instance().remove(dataKey);
    if(errorCode != StorageError::Code::EC_OK)
    {
        return false;
    }

    errorCode = Storage::instance().remove(dataKey);
    if(errorCode != StorageError::Code::EC_DELETED)
    {
        return false;
    }

    const QPair<DataKey, QByteArray> &selection = Storage::instance().select(dataKey.shmKey, dataKey.index);
    if((selection.first.shmKey != dataKey.shmKey) ||
       (selection.first.index != dataKey.index) ||
       (selection.first.errorCode != StorageError::Code::EC_DATA_NOT_AVAILABLE_IN_INDEX))
    {
        return false;
    }

    return true;
}

bool StorageTest_updateDataSizeTooBig_valid()
{
    QString storageKey = TestsParams::StorageDescriptor;
    int firstInvalidIndex = 0;

    StorageError::Code errorCode = Storage::instance().update(DataKey(storageKey, firstInvalidIndex),
                                                     QByteArray(TestsParams::InsertDataTooBig));
    if(errorCode != StorageError::Code::EC_INVALID_CHUNK_DATA_SIZE)
    {
        return false;
    }

    return true;
}

bool StorageTest_updateOutOfBoundIndex_valid()
{
    QString storageKey = TestsParams::StorageDescriptor;
    int firstInvalidIndex = Storage::instance().storageChunksCount(storageKey);

    StorageError::Code errorCode = Storage::instance().update(DataKey(storageKey, firstInvalidIndex),
                                                     QByteArray(TestsParams::UpdateData));
    if(errorCode != StorageError::Code::EC_INDEX_OUT_OF_RANGE)
    {
        return false;
    }

    return true;
}

bool StorageTest_updateNegativeIndex_valid()
{
    QString storageKey = TestsParams::StorageDescriptor;
    int updateIndex = -5;

    StorageError::Code errorCode = Storage::instance().update(DataKey(storageKey, updateIndex),
                                                     QByteArray(TestsParams::UpdateData));
    if(errorCode != StorageError::Code::EC_INDEX_IS_NEGATIVE)
    {
        return false;
    }

    return true;
}

bool StorageTest_updateInvalidKey_valid()
{
    QString storageKey = TestsParams::InvalidKey;
    int updateIndex = 0;

    StorageError::Code errorCode = Storage::instance().update(DataKey(storageKey, updateIndex),
                                                     QByteArray(TestsParams::UpdateData));
    if(errorCode != StorageError::Code::EC_KEY_NOT_AVAILABLE)
    {
        return false;
    }

    return true;
}

bool StorageTest_update_valid()
{
    QString storageKey = TestsParams::StorageDescriptor;
    int updateIndex = 0;

    Storage::instance().removeAll(storageKey);
    Storage::instance().insert(storageKey, QByteArray(TestsParams::InsertData));

    StorageError::Code errorCode = Storage::instance().update(DataKey(storageKey, updateIndex),
                                                     QByteArray(TestsParams::UpdateData));
    if(errorCode != StorageError::Code::EC_OK)
    {
        return false;
    }

    int expectedDataSize = strlen(TestsParams::UpdateData);
    const QPair<DataKey, QByteArray> &selection = Storage::instance().select(storageKey, updateIndex);
    if((selection.first.shmKey != storageKey) ||
       (selection.first.index != updateIndex) ||
       (selection.first.errorCode != StorageError::Code::EC_OK) ||
       (selection.second.size() != expectedDataSize) ||
       (memcmp(TestsParams::UpdateData, selection.second.constData(), expectedDataSize) != 0))
    {
        return false;
    }

    Storage::instance().removeAll(storageKey);
    return true;
}

bool StorageTest_selectIndexesList_valid()
{
    QString storageKey = TestsParams::StorageDescriptor;
    Storage::instance().removeAll(storageKey);

    int chunksCount = Storage::instance().storageChunksCount(storageKey);
    for(int recordNum = 0; recordNum < chunksCount; recordNum++)
    {
        const DataKey &dataKey = Storage::instance().insert(storageKey, QByteArray(TestsParams::InsertData));
        if((dataKey.shmKey != storageKey) ||
           (dataKey.index != recordNum) ||
           (dataKey.errorCode != StorageError::Code::EC_OK))
        {
            return false;
        }
    }

    int expectedDataSize = strlen(TestsParams::InsertData);
    QList<int> indexes;

    for(int index = 0; index < chunksCount; index++)
    {
        indexes << index;
    }

    const QList<QPair<DataKey, QByteArray>> &selection = Storage::instance().select(storageKey, indexes);
    if(selection.size() != chunksCount)
    {
        return false;
    }

    for(int index = 0; index < chunksCount; index++)
    {
        if((selection[index].first.shmKey != storageKey) ||
           (selection[index].first.index != index) ||
           (selection[index].first.errorCode != StorageError::Code::EC_OK) ||
           (selection[index].second.size() != expectedDataSize) ||
           (memcmp(TestsParams::InsertData, selection[index].second.constData(), expectedDataSize) != 0))
        {
            return false;
        }
    }

    Storage::instance().removeAll(storageKey);
    return true;
}

bool StorageTest_selectAll_valid()
{
    QString storageKey = TestsParams::StorageDescriptor;
    Storage::instance().removeAll(storageKey);

    int chunksCount = Storage::instance().storageChunksCount(storageKey);
    for(int recordNum = 0; recordNum < chunksCount; recordNum++)
    {
        const DataKey &dataKey = Storage::instance().insert(storageKey, QByteArray(TestsParams::InsertData));
        if((dataKey.shmKey != storageKey) ||
           (dataKey.index != recordNum) ||
           (dataKey.errorCode != StorageError::Code::EC_OK))
        {
            return false;
        }
    }

    int expectedDataSize = strlen(TestsParams::InsertData);
    QList<int> indexes;

    for(int index = 0; index < chunksCount; index++)
    {
        indexes << index;
    }

    const QList<QPair<DataKey, QByteArray>> &selection = Storage::instance().selectAll(storageKey);
    if(selection.size() != chunksCount)
    {
        return false;
    }

    for(int index = 0; index < chunksCount; index++)
    {
        if((selection[index].first.shmKey != storageKey) ||
           (selection[index].first.index != index) ||
           (selection[index].first.errorCode != StorageError::Code::EC_OK) ||
           (selection[index].second.size() != expectedDataSize) ||
           (memcmp(TestsParams::InsertData, selection[index].second.constData(), expectedDataSize) != 0))
        {
            return false;
        }
    }

    Storage::instance().removeAll(storageKey);
    return true;
}

bool StorageTest_selectAllInvalidKey_valid()
{
    QString storageKey = TestsParams::InvalidKey;
    const QList<QPair<DataKey, QByteArray>> &selection = Storage::instance().selectAll(storageKey);
    if((selection.size() != 1) ||
       (selection[0].first.shmKey != storageKey) ||
       (selection[0].first.index != -1) ||
       (selection[0].first.errorCode != StorageError::Code::EC_KEY_NOT_AVAILABLE))
    {
        return false;
    }

    return true;
}

bool StorageTest_selectOutOfBoundIndex_valid()
{
    QString storageKey = TestsParams::StorageDescriptor;
    int firstInvalidIndex = Storage::instance().storageChunksCount(storageKey);
    const QPair<DataKey, QByteArray> &selection = Storage::instance().select(storageKey, firstInvalidIndex);
    if((selection.first.shmKey != storageKey) ||
       (selection.first.index != firstInvalidIndex) ||
       (selection.first.errorCode != StorageError::Code::EC_INDEX_OUT_OF_RANGE))
    {
        return false;
    }

    return true;
}

bool StorageTest_selectByDataKeyOutOfBoundIndex_valid()
{
    QString storageKey = TestsParams::StorageDescriptor;
    int firstInvalidIndex = Storage::instance().storageChunksCount(storageKey);
    const DataKey &dataKey = DataKey(storageKey, firstInvalidIndex, StorageError::Code::EC_UNDEFINED);
    const QPair<DataKey, QByteArray> &selection = Storage::instance().select(dataKey);
    if((selection.first.shmKey != storageKey) ||
       (selection.first.index != firstInvalidIndex) ||
       (selection.first.errorCode != StorageError::Code::EC_INDEX_OUT_OF_RANGE))
    {
        return false;
    }

    return true;
}

bool StorageTest_selectNegativeIndex_valid()
{
    QString storageKey = TestsParams::StorageDescriptor;
    int recordIndex = -5;
    const QPair<DataKey, QByteArray> &selection = Storage::instance().select(storageKey, recordIndex);
    if((selection.first.shmKey != storageKey) ||
       (selection.first.index != recordIndex) ||
       (selection.first.errorCode != StorageError::Code::EC_INDEX_IS_NEGATIVE))
    {
        return false;
    }

    return true;
}

bool StorageTest_selectByDataKeyNegativeIndex_valid()
{
    QString storageKey = TestsParams::StorageDescriptor;
    int recordIndex = -5;
    const DataKey &dataKey = DataKey(storageKey, recordIndex, StorageError::Code::EC_UNDEFINED);
    const QPair<DataKey, QByteArray> &selection = Storage::instance().select(dataKey);
    if((selection.first.shmKey != storageKey) ||
       (selection.first.index != recordIndex) ||
       (selection.first.errorCode != StorageError::Code::EC_INDEX_IS_NEGATIVE))
    {
        return false;
    }

    return true;
}

bool StorageTest_selectInvalidKey_valid()
{
    QString storageKey = TestsParams::InvalidKey;
    int recordIndex = 0;
    const QPair<DataKey, QByteArray> &selection = Storage::instance().select(storageKey, recordIndex);
    if((selection.first.shmKey != storageKey) ||
       (selection.first.index != -1) ||
       (selection.first.errorCode != StorageError::Code::EC_KEY_NOT_AVAILABLE))
    {
        return false;
    }

    return true;
}

bool StorageTest_selectByDataKeyInvalidKey_valid()
{
    QString storageKey = TestsParams::InvalidKey;
    int recordIndex = 0;
    const DataKey &dataKey = DataKey(storageKey, recordIndex, StorageError::Code::EC_UNDEFINED);
    const QPair<DataKey, QByteArray> &selection = Storage::instance().select(dataKey);
    if((selection.first.shmKey != storageKey) ||
       (selection.first.index != -1) ||
       (selection.first.errorCode != StorageError::Code::EC_KEY_NOT_AVAILABLE))
    {
        return false;
    }

    return true;
}

bool StorageTest_selectIndex_valid()
{
    QString storageKey = TestsParams::StorageDescriptor;
    Storage::instance().removeAll(storageKey);

    int chunksCount = Storage::instance().storageChunksCount(storageKey);
    for(int recordNum = 0; recordNum < chunksCount; recordNum++)
    {
        const DataKey &dataKey = Storage::instance().insert(storageKey, QByteArray(TestsParams::InsertData));
        if((dataKey.shmKey != storageKey) ||
           (dataKey.index != recordNum) ||
           (dataKey.errorCode != StorageError::Code::EC_OK))
        {
            return false;
        }
    }

    int recordIndex = 0;
    int expectedDataSize = strlen(TestsParams::InsertData);
    const QPair<DataKey, QByteArray> &selection = Storage::instance().select(storageKey, recordIndex);
    if((selection.first.shmKey != storageKey) ||
       (selection.first.index != recordIndex) ||
       (selection.first.errorCode != StorageError::Code::EC_OK) ||
       (selection.second.size() != expectedDataSize) ||
       (memcmp(TestsParams::InsertData, selection.second.constData(), expectedDataSize) != 0))
    {
        return false;
    }

    Storage::instance().removeAll(storageKey);
    return true;
}

bool StorageTest_selectByDataKeyIndex_valid()
{
    QString storageKey = TestsParams::StorageDescriptor;
    Storage::instance().removeAll(storageKey);

    int chunksCount = Storage::instance().storageChunksCount(storageKey);
    for(int recordNum = 0; recordNum < chunksCount; recordNum++)
    {
        const DataKey &dataKey = Storage::instance().insert(storageKey, QByteArray(TestsParams::InsertData));
        if((dataKey.shmKey != storageKey) ||
           (dataKey.index != recordNum) ||
           (dataKey.errorCode != StorageError::Code::EC_OK))
        {
            return false;
        }
    }

    int recordIndex = 0;
    int expectedDataSize = strlen(TestsParams::InsertData);
    const DataKey &dataKey = DataKey(storageKey, recordIndex, StorageError::Code::EC_UNDEFINED);
    const QPair<DataKey, QByteArray> &selection = Storage::instance().select(dataKey);
    if((selection.first.shmKey != storageKey) ||
       (selection.first.index != recordIndex) ||
       (selection.first.errorCode != StorageError::Code::EC_OK) ||
       (selection.second.size() != expectedDataSize) ||
       (memcmp(TestsParams::InsertData, selection.second.constData(), expectedDataSize) != 0))
    {
        return false;
    }

    Storage::instance().removeAll(storageKey);
    return true;
}

bool StorageTest_insertDataSizeTooBig_valid()
{
    QString storageKey = TestsParams::StorageDescriptor;
    const DataKey &dataKey = Storage::instance().insert(storageKey, QByteArray(TestsParams::InsertDataTooBig));
    if((dataKey.shmKey != storageKey) ||
       (dataKey.index != -1) ||
       (dataKey.errorCode != StorageError::Code::EC_DATA_SIZE_TOO_BIG))
    {
        return false;
    }

    return true;
}

bool StorageTest_insertInvalidKey_valid()
{
    const DataKey &dataKey = Storage::instance().insert(TestsParams::InvalidKey,
                                                        QByteArray(TestsParams::InsertData));
    if((dataKey.shmKey != TestsParams::InvalidKey) ||
       (dataKey.index != -1) ||
       (dataKey.errorCode != StorageError::Code::EC_KEY_NOT_AVAILABLE))
    {
        return false;
    }

    return true;
}

bool StorageTest_insert_valid()
{
    QString storageKey = TestsParams::StorageDescriptor;
    Storage::instance().removeAll(storageKey);

    int chunksCount = Storage::instance().storageChunksCount(storageKey);
    for(int recordNum = 0; recordNum < chunksCount; recordNum++)
    {
        const DataKey &dataKey = Storage::instance().insert(storageKey, QByteArray(TestsParams::InsertData));
        if((dataKey.shmKey != storageKey) ||
           (dataKey.index != recordNum) ||
           (dataKey.errorCode != StorageError::Code::EC_OK))
        {
            return false;
        }
    }

    const DataKey &dataKey = Storage::instance().insert(TestsParams::StorageDescriptor,
                                                        QByteArray(TestsParams::InsertData));
    if((dataKey.shmKey != storageKey) ||
       (dataKey.index != chunksCount) ||
       (dataKey.errorCode != StorageError::Code::EC_NO_FREE_SPACE))
    {
        return false;
    }

    Storage::instance().removeAll(TestsParams::StorageDescriptor);
    return true;
}

bool StorageTest_storageChunksCount_valid()
{
    int storage1ChunksCount = Storage::instance().storageCommonSize(TestsParams::StorageDescriptor) /
                                    TestsParams::STORAGE_1_CHUNKS_SIZE;

    if(Storage::instance().storageChunksCount(TestsParams::StorageDescriptor) != storage1ChunksCount)
    {
        return false;
    }

    return true;
}

bool StorageTest_storageChunksSize_valid()
{
    if((Storage::instance().storageChunksSize(TestsParams::StorageDescriptor) !=
            TestsParams::STORAGE_1_CHUNKS_SIZE))

    {
        return false;
    }

    return true;
}

bool StorageTest_storageCommonSize_valid()
{
    int storage1Size = Storage::instance().storageCommonSize(TestsParams::StorageDescriptor);

    if(storage1Size < TestsParams::STORAGE_1_CAPACITY_BYTES)
    {
        return false;
    }

    return true;
}

bool StorageTest_storagesKeys_valid()
{
    const QList<QString> &keys = Storage::instance().storagesKeys();
    if(!keys.contains(TestsParams::StorageDescriptor.toLower()))
    {
        return false;
    }

    return true;
}

bool StorageTest_getNotificationPort_valid()
{
    return Storage::instance().getNotificationPort() == SettingsParams::DEFAULT_NOTIFICATION_PORT;
}

