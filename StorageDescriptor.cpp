#include "StorageDescriptor.h"

StorageDescriptor::StorageDescriptor():
    description(""),
    name(""),
    size(-1),
    rowSize(-1),
    segmentId(-1),
    type(StorageType::UNDEFINED)
{}

QString StorageDescriptor::storageTypeToString(StorageType type) const
{
    if(type == StorageType::FIXED)
    {
        return "fixed";
    }
    else if(type == StorageType::SCALABLE)
    {
        return "scalable";
    }

    return "undefined";
}

QString StorageDescriptor::toString() const
{
    return QString("name: '%1', description: '%2', size: '%3', rowSize: '%4', type: '%5'").
            arg(name).arg(description).arg(size).arg(rowSize).arg(storageTypeToString(type));
}

