#ifndef EVENT_H
#define EVENT_H

#include "DataKey.h"
#include "Utils.h"

enum class StorageOpCode: int
{
    INSERT,
    SELECT,
    SELECT_ALL,
    UPDATE,
    REMOVE,
    REMOVE_ALL,
    UNDEFINED,
};

QString storageOpCodeToString(StorageOpCode opCode);

struct UserIdentificator
{
    QString userName;
    int userId;

    UserIdentificator():
        userName("System"),
        userId(-1)
    {}

    UserIdentificator(const QString &userName, int userId):
        userName(userName),
        userId(userId)
    {}
};

#pragma pack(push, 1)
struct StorageEventInfo
{
    enum StorageEventParams
    {
        HOST_ADDR_SIZE      = 4,
        SOURCE_NAME_LENGTH  = 16,
        SHM_KEY_LENGTH      = 16,

        RESERVED_SIZE       = 14,
    };

    StorageOpCode operationCode;        // Код операции
    quint64 nanoSecsSinceEpoch;         // Наносекунды с начала эпохи
    char appName[SOURCE_NAME_LENGTH];   // Имя приложения-источника события
    uint8_t hostAddr[HOST_ADDR_SIZE];   // Адрес хоста
    char shmKey[SHM_KEY_LENGTH];        // Имя SharedMemory
    int dataCellIndex;                  // Индекс ячейки данных
    StorageError::Code errorCode;       // Код ошибки операции
    qint64 pid;                         // PID процесса
    uint16_t payloadSize;               // Размер полезных данных
    char reserved[RESERVED_SIZE];       // Зарезервировано

    StorageEventInfo();

    DataKey getDataKey() const;
    void setDataKey(const DataKey &dataKey);
    void setAppName(const QString &appName);
    QString getAppName() const;
    QString opCodeString() const;
    void setHostAddress(const Utils::IPv4Address address);
    void setHostAddress(uint32_t address);
    void setShmKey(const QString &shmKey);
    QString getShmKey();
};
#pragma pack(pop)

#pragma pack(push, 1)
struct JournalEventInfo: public StorageEventInfo
{
    enum JournalEventParams
    {
        USER_NAME_LENGTH    = 16,
        MESSAGE_TYPE_LENGTH = 16,

        RESERVED_SIZE       = 14,
    };

    int userId;
    char userName[USER_NAME_LENGTH];
    char messageType[MESSAGE_TYPE_LENGTH];
    char reserved[RESERVED_SIZE];

    JournalEventInfo();
    JournalEventInfo(const DataKey &dataKey,
                     const UserIdentificator &userId,
                     const QString &messageType,
                     StorageOpCode opCode);
    void setUserName(const QString &userName);
    QString getUserName() const;
    void setMessageType(const QString &messageType);
    QString getMessageType() const;
};
#pragma pack(pop)

struct StorageEvent
{
    StorageEvent(const StorageEventInfo &eventInfo, const char* data):
        eventInfo(eventInfo),
        data(data, eventInfo.payloadSize)
    {}

    StorageEventInfo eventInfo;
    QByteArray data;
};

struct JournalEvent
{
    JournalEvent(const JournalEventInfo &eventInfo, const char* data):
        eventInfo(eventInfo),
        data(data, eventInfo.payloadSize)
    {}

    JournalEventInfo eventInfo;
    QByteArray data;
    void setMessage(const QString &message);
    QByteArray toByteArray();
};

struct JournalEventStruct
{
    StorageOpCode opCode;
    quint64 operationTime;
    int userId;
    DataKey *dataKey;
    QByteArray *data;
    QString *userName;
    QString *messageType;
    QString *message;

    JournalEventStruct():
        dataKey(nullptr),
        data(nullptr),
        userName(nullptr),
        messageType(nullptr),
        message(nullptr)
    {}
};

#endif // EVENT_H
