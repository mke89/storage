#ifndef STORAGEDESCRIPTOR_H
#define STORAGEDESCRIPTOR_H

#include <QString>

struct StorageDescriptor
{
    enum StorageType
    {
        FIXED,
        SCALABLE,
        UNDEFINED,
    };

    QString description;
    QString name;
    int size;
    int rowSize;
    int segmentId;
    StorageType type;

    StorageDescriptor();
    QString storageTypeToString(StorageType type) const;
    QString toString() const;
};

#endif // STORAGEDESCRIPTOR_H
