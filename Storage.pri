isEmpty(STORAGE_INCLUDED) {
# include this as .PRI rather than as dynamic library, if you want string lliterals translated.
STORAGE_INCLUDED = 1
INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

SOURCES += \
    $$PWD/Storage.cpp \
    $$PWD/Settings.cpp \
    $$PWD/SEvent.cpp \
    $$PWD/DataKey.cpp \
    $$PWD/Utils.cpp \
    $$PWD/StorageDescriptor.cpp \
    $$PWD/JournalListener.cpp \
    $$PWD/JournalSender.cpp

HEADERS  += \
    $$PWD/Storage.h \
    $$PWD/Settings.h \
    $$PWD/SEvent.h \
    $$PWD/DataKey.h \
    $$PWD/Utils.h \
    $$PWD/StorageDescriptor.h \
    $$PWD/JournalListener.h \
    $$PWD/JournalSender.h
}



