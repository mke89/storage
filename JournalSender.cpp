#include "JournalSender.h"
#include "Settings.h"

QString JournalSender::confFileName = "SharedMemory";

JournalSender::JournalSender():
    journalPort(0),
    journalSocket(nullptr)
{
    journalPort = loadJournalPort();
    if(!journalPort)
    {
        return;
    }

    journalSocket = initJournalSocket(journalPort);
    if(!journalSocket)
    {
        return;
    }

    sendEvent("Journal sender initialised");
}

JournalSender::~JournalSender()
{
}

JournalSender& JournalSender::instance()
{
    static JournalSender instance;
    return instance;
}

quint16 JournalSender::loadJournalPort()
{
    const QList<QString> &keys = StorageSettings::Settings::instance(confFileName).
                                        groupKeys(SettingsParams::NETWORK_GROUP);
    QString journalPortString;
    bool containsJournalPort = false;

    for(const QString &key: keys)
    {
        if(key.toLower() == SettingsParams::JOURNAL_PORT.toLower())
        {
            containsJournalPort = true;
            journalPortString = key;
            break;
        }
    }

    if(!containsJournalPort)
    {
        qDebug() << "JournalSender ERROR: journal port key not found...";
        return 0;
    }

    bool ok = false;
    quint16 port = StorageSettings::Settings::instance(confFileName).value(SettingsParams::NETWORK_GROUP + "/" +
                                                                           journalPortString).toUInt(&ok);
    if(!ok)
    {
        qDebug() << "JournalSender ERROR: journal port parsing error...";
        return 0;
    }

    return port;
}

QUdpSocket* JournalSender::initJournalSocket(quint16 journalPort)
{
    if(journalPort == 0)
    {
        qDebug() << "JournalSender ERROR: incorrect journal port...";
        return nullptr;
    }

    QUdpSocket* journalSocket = new QUdpSocket();
    if(!journalSocket)
    {
        qDebug() << "JournalSender ERROR: journal socket allocation error...";
        return nullptr;
    }

    if(!journalSocket->bind(QHostAddress::Broadcast, journalPort, QAbstractSocket::ReuseAddressHint))
    {
        qDebug() << "JournalSender ERROR: journal socket bind error...";
        printUdpSocketError(journalSocket);
        return nullptr;
    }

    qDebug() << "JournalSender: journal socket initialisation OK...";
    return journalSocket;
}

void JournalSender::printUdpSocketError(const QUdpSocket *socket)
{
    QString errorDecsription = QString("Error: %1; errorString: '%2'").
            arg(socket->error()).arg(socket->errorString());
    qDebug() << errorDecsription.toLatin1().constData();
}

quint16 JournalSender::getJournalPort() const
{
    return journalPort;
}

bool JournalSender::sendEvent(const JournalEvent &event)
{
    if(!journalSocket)
    {
        qDebug() << "JournalSender sendEvent() ERROR: journal socket is not initialised...";
        return false;
    }

    if(event.eventInfo.payloadSize != event.data.size())
    {
        qDebug() << "JournalSender sendEvent() ERROR: payloadSize is not equal data size...";
        return false;
    }

    int headerSize = sizeof(JournalEventInfo);
    int commonDataLength = headerSize + event.data.size();
    int bufferSize = commonDataLength + 10;
    char buffer[bufferSize];

    memcpy(buffer, &event.eventInfo, headerSize);
    if(event.eventInfo.payloadSize)
    {
        memcpy(buffer + headerSize, event.data.constData(), event.data.size());
    }

    int dataSent = journalSocket->writeDatagram(buffer, commonDataLength,
                                                QHostAddress::Broadcast, journalPort);

    if(dataSent != commonDataLength)
    {
        qDebug() << "JournalSender sendEvent() ERROR: incorrect sended data size...";
        qDebug() << "JournalSender sendEvent() ERROR: real data sent: " << dataSent << "...";
        qDebug() << "JournalSender sendEvent() ERROR: expected data sent: " << commonDataLength << "...";
        printUdpSocketError(journalSocket);
        return false;
    }

    return true;
}

bool JournalSender::sendEvent(const JournalEventInfo &eventInfo, const QString &message)
{
    if(!journalSocket)
    {
        qDebug() << "JournalSender sendEvent() ERROR: journal socket is not initialised...";
        return false;
    }

    if(message.isEmpty())
    {
        qDebug() << "JournalSender sendEvent() ERROR: journal message is empty...";
        return false;
    }

    int headerSize = sizeof(JournalEventInfo);
    int commonDataLength = headerSize + message.size();
    int bufferSize = commonDataLength + 10;
    char buffer[bufferSize];

    memcpy(buffer, &eventInfo, headerSize);
    JournalEventInfo *evtInfo = (JournalEventInfo*)buffer;
    evtInfo->payloadSize = message.size();
    memcpy(buffer + headerSize, message.toLocal8Bit().constData(), message.size());

    int dataSent = journalSocket->writeDatagram(buffer, commonDataLength,
                                                QHostAddress::Broadcast, journalPort);

    if(dataSent != commonDataLength)
    {
        qDebug() << "JournalSender sendEvent() ERROR: incorrect sended data size...";
        qDebug() << "JournalSender sendEvent() ERROR: real data sent: " << dataSent << "...";
        qDebug() << "JournalSender sendEvent() ERROR: expected data sent: " << commonDataLength << "...";
        printUdpSocketError(journalSocket);
        return false;
    }

    return true;
}

bool JournalSender::sendEvent(const QString &message, const QString &messageType)
{
    if(!journalSocket)
    {
        qDebug() << "JournalSender sendEvent() ERROR: journal socket is not initialised...";
        return false;
    }

    if(message.isEmpty())
    {
        qDebug() << "JournalSender sendEvent() ERROR: journal message is empty...";
        return false;
    }

    JournalEventInfo eventInfo;
    int headerSize = sizeof(JournalEventInfo);
    int commonDataLength = headerSize + message.size();
    int bufferSize = commonDataLength + 10;
    char buffer[bufferSize];

    memcpy(buffer, &eventInfo, headerSize);
    JournalEventInfo *evtInfo = (JournalEventInfo*)buffer;
    evtInfo->setMessageType(messageType);
    evtInfo->payloadSize = message.size();
    memcpy(buffer + headerSize, message.toLocal8Bit().constData(), message.size());

    int dataSent = journalSocket->writeDatagram(buffer, commonDataLength,
                                                QHostAddress::Broadcast, journalPort);

    if(dataSent != commonDataLength)
    {
        qDebug() << "JournalSender sendEvent() ERROR: incorrect sended data size...";
        qDebug() << "JournalSender sendEvent() ERROR: real data sent: " << dataSent << "...";
        qDebug() << "JournalSender sendEvent() ERROR: expected data sent: " << commonDataLength << "...";
        printUdpSocketError(journalSocket);
        return false;
    }

    return true;
}

bool JournalSender::sendEvent(const UserIdentificator &userIdentificator, const QString &message, const QString &messageType)
{
    if(!journalSocket)
    {
        qDebug() << "JournalSender sendEvent() ERROR: journal socket is not initialised...";
        return false;
    }

    if(message.isEmpty())
    {
        qDebug() << "JournalSender sendEvent() ERROR: journal message is empty...";
        return false;
    }

    JournalEventInfo eventInfo;
    int headerSize = sizeof(JournalEventInfo);
    int commonDataLength = headerSize + message.size();
    int bufferSize = commonDataLength + 10;
    char buffer[bufferSize];

    memcpy(buffer, &eventInfo, headerSize);
    JournalEventInfo *evtInfo = (JournalEventInfo*)buffer;
    evtInfo->payloadSize = message.size();
    evtInfo->setUserName(userIdentificator.userName);
    evtInfo->userId = userIdentificator.userId;
    evtInfo->setMessageType(messageType);
    memcpy(buffer + headerSize, message.toLocal8Bit().constData(), message.size());

    int dataSent = journalSocket->writeDatagram(buffer, commonDataLength,
                                                QHostAddress::Broadcast, journalPort);

    if(dataSent != commonDataLength)
    {
        qDebug() << "JournalSender sendEvent() ERROR: incorrect sended data size...";
        qDebug() << "JournalSender sendEvent() ERROR: real data sent: " << dataSent << "...";
        qDebug() << "JournalSender sendEvent() ERROR: expected data sent: " << commonDataLength << "...";
        printUdpSocketError(journalSocket);
        return false;
    }

    return true;
}



