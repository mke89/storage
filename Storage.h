#ifndef STORAGE_H
#define STORAGE_H

#include <QString>
#include <QByteArray>
#include <QPair>
#include <QList>
#include <QSharedMemory>
#include <QHash>
#include <QUdpSocket>
#include <QCoreApplication>
#include <QSystemSemaphore>
#include "DataKey.h"
#include "SEvent.h"
#include "StorageDescriptor.h"


class Storage: public QObject
{
    Q_OBJECT

    public:
        typedef int StorageSize;
        typedef int ChunksSize;

        enum class Parameters: int
        {
            HEADER_SIZE         = sizeof(int)
        };

        static Storage &instance();
        QList<QString> storagesKeys() const;
        QList<StorageDescriptor> getStoragesDescriptors();
        int storageChunksCount(const QString &key) const;
        int storageUsedChunksCount(const QString &key);
        int storageChunksSize(const QString &key) const;
        int storageCommonSize(const QString &key) const;
        int getNativeKey(const QString &key) const;
        quint16 getNotificationPort() const;
        bool receiveOwnNotifications() const;
        void setOwnNotificationsReception(bool receive);
        bool displayErrors() const;
        void setErrorsDisplaying(bool display);

        DataKey insert(const QString &key, const QByteArray &data);
        QPair<DataKey, QByteArray> select(const QString &key, int index);
        QPair<DataKey, QByteArray> select(const DataKey &dataIndex);
        QList<QPair<DataKey, QByteArray>> select(const QString &key, const QList<int> &indexes);
        QList<QPair<DataKey, QByteArray>> selectAll(const QString &key);
        StorageError::Code update(const DataKey &dataIndex, const QByteArray &data);
        StorageError::Code remove(const DataKey &dataIndex);
        StorageError::Code removeAll(const QString &key);

    private:
        Storage();
        Storage(const Storage& object);
        Storage& operator=(const Storage&);
        ~Storage();

        void initSharedStorages();
        void markupStorage(const QString &key, bool created);
        bool initSocket(quint16 port, QUdpSocket **socket, const QString &sockDestination);
        void initInternals();
        void printSharedMemoryError(const QSharedMemory *shMem);
        void printUdpSocketError(const QUdpSocket *socket);
        void printSystemSemaphoreError(const QSystemSemaphore *semaphore);
        void setNativeKey(const StorageDescriptor &storage, const QString &nativeKey);

        quint16 loadPort(const QString &portKey, const QString &portName);
        int getMaxDataSize(const QList<QPair<DataKey, QByteArray>> &records) const;
        StorageError::Code postEvents(StorageOpCode opCode, const QList<quint64> &operationTimes,
                                    const QList<QPair<DataKey, QByteArray>> &eventDataRecords) const;
        StorageError::Code postEvent(StorageOpCode opCode, quint64 operationTime,
                                    const DataKey &dataKey, const QByteArray &data) const;

        QMap<QString, QSharedMemory*> sharedStorages;
        QHash<QString, int> sharedStoragesSizes;
        QHash<QString, int> dataChunksSizes;
        QHash<QString, int> dataChunksCounts;
        QHash<QString, int> nativeKeys;

        QHash<QString, QVector<int*>> chunksBytesAvailable;
        QHash<QString, QVector<char*>> chunksDataPtrs;

        quint16 notificationPort;
        QUdpSocket *notificationSocket;
        QSharedMemory *mainSharedMemory;
        bool receiveOwnEvents;
        bool displayErrorsInfo;

        static QString confFileName;
        static QString messageTypeStorage;

    private slots:
        void readPendingDatagrams();

    signals:
        void eventReceived(StorageEvent event);
};

#endif // STORAGE_H
