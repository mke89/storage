#include "Storage.h"
#include "Settings.h"
#include "Utils.h"
#include "JournalSender.h"
#include <QSystemSemaphore>
#include <QNetworkDatagram>
#include <QNetworkInterface>
#include <QCoreApplication>
#include <QFile>
#include <sys/ipc.h>

QString Storage::confFileName = "SharedMemory";
QString Storage::messageTypeStorage = "Storage";

Storage::Storage():
    notificationSocket(nullptr),
    mainSharedMemory(nullptr),
    receiveOwnEvents(false),
    displayErrorsInfo(true)
{
    JournalSender::instance().sendEvent("Storage initialising...", messageTypeStorage);
    const QString &confFile = QString("Configuration file: %1.conf").arg(confFileName);
    JournalSender::instance().sendEvent(confFile, "Storage");
    qDebug() << confFile.toLatin1().constData();
    StorageSettings::Settings::instance(confFileName);
    initInternals();

    QSystemSemaphore systemSemaphore("InitSemaphore", 1);
    JournalSender::instance().sendEvent("System semaphore acquiring...", messageTypeStorage);
    if(!systemSemaphore.acquire())
    {
        JournalSender::instance().sendEvent("System semaphore acquiring error...", messageTypeStorage);
        printSystemSemaphoreError(&systemSemaphore);
        return;
    }

    QSharedMemory *shMemPtr = new QSharedMemory("InitSharedMemory");
    JournalSender::instance().sendEvent("System shared memory attaching...", messageTypeStorage);
    if(shMemPtr->attach())
    {
        JournalSender::instance().sendEvent("System shared memory attached...", messageTypeStorage);
        initSharedStorages();
        if(!systemSemaphore.release())
        {
            JournalSender::instance().sendEvent("System semaphore release error...", messageTypeStorage);
            printSystemSemaphoreError(&systemSemaphore);
        }
        else
        {
            JournalSender::instance().sendEvent("System semaphore released...", messageTypeStorage);
        }
        mainSharedMemory = shMemPtr;
        return;
    }
    else
    {
        JournalSender::instance().sendEvent("System shared memory attach error...", messageTypeStorage);
    }

    JournalSender::instance().sendEvent("System shared memory creating...", messageTypeStorage);
    if(!shMemPtr->create(128))
    {
        delete shMemPtr;
        if(!systemSemaphore.release())
        {
            JournalSender::instance().sendEvent("System semaphore release error...", messageTypeStorage);
            printSystemSemaphoreError(&systemSemaphore);
        }
        JournalSender::instance().sendEvent("System shared memory creating error...", messageTypeStorage);
        qDebug() << "Main shared memory creation error...";
        printSharedMemoryError(shMemPtr);
        return;
    }
    else
    {
        JournalSender::instance().sendEvent("System shared memory create success...", messageTypeStorage);
    }

    initSharedStorages();
    if(!systemSemaphore.release())
    {
        JournalSender::instance().sendEvent("System semaphore release error...", messageTypeStorage);
        printSystemSemaphoreError(&systemSemaphore);
    }
    mainSharedMemory = shMemPtr;
    return;
}

void Storage::initInternals()
{
    notificationPort = loadPort(SettingsParams::NOTIFICATION_PORT, "Notification");
    initSocket(notificationPort, &notificationSocket, "Notification");
}

void Storage::initSharedStorages()
{
    JournalSender::instance().sendEvent("Storages initialising...", messageTypeStorage);
    const QList<StorageDescriptor> &storages = getStoragesDescriptors();
    for(const StorageDescriptor &storage: storages)
    {
        QString infoString = QString("Storage: %1; size: %2; chunk size: %3;").
                arg(storage.name).arg(storage.size).arg(storage.rowSize);

        if(storage.size <= 0)
        {
            infoString.append(" Size error. Incorrect storage size...");
            JournalSender::instance().sendEvent(infoString, messageTypeStorage);
            qDebug() << infoString.toLatin1().constData();
            continue;
        }

        if(storage.rowSize <= 0)
        {
            infoString.append(" Size error. Incorrect storage chunk size...");
            JournalSender::instance().sendEvent(infoString, messageTypeStorage);
            qDebug() << infoString.toLatin1().constData();
            continue;
        }

        if(storage.rowSize > storage.size)
        {
            infoString.append(" Size error. chunk size is greater then common storage size...");
            JournalSender::instance().sendEvent(infoString, messageTypeStorage);
            qDebug() << infoString.toLatin1().constData();
            continue;
        }

        const QString &lowerCaseKey = storage.name.toLower();
        QSharedMemory *shMemPtr = new QSharedMemory(lowerCaseKey);
        if(!shMemPtr)
        {
            infoString.append(" Allocation error...");
            JournalSender::instance().sendEvent(infoString, messageTypeStorage);
            qDebug() << infoString.toLatin1().constData();
            continue;
        }

        if(shMemPtr->attach())
        {
            infoString.append(" Attached: OK...");
            JournalSender::instance().sendEvent(infoString, messageTypeStorage);
            qDebug() << infoString.toLatin1().constData();
            sharedStorages[lowerCaseKey] = shMemPtr;
            sharedStoragesSizes[lowerCaseKey] = storage.size;
            dataChunksSizes[lowerCaseKey] = storage.rowSize;
            dataChunksCounts[lowerCaseKey] = storage.size / storage.rowSize;
            setNativeKey(storage, shMemPtr->nativeKey());

            markupStorage(lowerCaseKey, false);
            continue;
        }

        int chunksCount = dataChunksCounts[lowerCaseKey];
        int commonMemorySize = storage.size + (chunksCount * (int)Parameters::HEADER_SIZE);
        if(!shMemPtr->create(commonMemorySize))
        {
            infoString.append(" Creation: ERROR...");
            JournalSender::instance().sendEvent(infoString, messageTypeStorage);
            qDebug() << infoString.toLatin1().constData();
            printSharedMemoryError(shMemPtr);

            delete shMemPtr;
            continue;
        }

        setNativeKey(storage, shMemPtr->nativeKey());

        infoString.append(" Creation: OK...");
        JournalSender::instance().sendEvent(infoString, messageTypeStorage);
        qDebug() << infoString.toLatin1().constData();
        sharedStorages[lowerCaseKey] = shMemPtr;
        sharedStoragesSizes[lowerCaseKey] = storage.size;
        dataChunksSizes[lowerCaseKey] = storage.rowSize;
        dataChunksCounts[lowerCaseKey] = chunksCount;

        markupStorage(lowerCaseKey, true);
    }
}

void Storage::setNativeKey(const StorageDescriptor &storage, const QString &nativeKey)
{
    const QString &lowerCaseKey = storage.name.toLower();
    int intNativeKey = ftok(QFile::encodeName(nativeKey).constData(), 'Q');
    nativeKeys[lowerCaseKey] = intNativeKey;
    if(storage.segmentId != intNativeKey)
    {
        const QString &key = SettingsParams::STORAGES_PREAMBLE + storage.name +
                             "/" + SettingsParams::STORAGE_SEGMENT_ID;
        StorageSettings::Settings::instance(confFileName).setValue(key, intNativeKey);
    }
}

void Storage::printSharedMemoryError(const QSharedMemory *shMem)
{
    QString errorDecsription = QString("Error: %1; errorString: '%2'").
            arg(shMem->error()).arg(shMem->errorString());
    qDebug() << errorDecsription.toLatin1().constData();
}

void Storage::printUdpSocketError(const QUdpSocket *socket)
{
    QString errorDecsription = QString("Error: %1; errorString: '%2'").
            arg(socket->error()).arg(socket->errorString());
    qDebug() << errorDecsription.toLatin1().constData();
}

void Storage::printSystemSemaphoreError(const QSystemSemaphore *semaphore)
{
    QString errorDecsription = QString("Error: %1; errorString: '%2'").
            arg(semaphore->error()).arg(semaphore->errorString());
    qDebug() << errorDecsription.toLatin1().constData();
}

void Storage::markupStorage(const QString &key, bool created)
{
    if(!sharedStorages.contains(key))
    {
        qDebug() << "Storage: " << key << " markup error...";
        return;
    }
    char *data = (char*)sharedStorages[key]->data();
    int chunksCount = dataChunksCounts[key];
    QVector<int*> chunksHeadersPtrs(chunksCount);
    QVector<char*> chunksDataPtrs(chunksCount);
    for(int currentChunk = 0; currentChunk < chunksCount; currentChunk++)
    {
        chunksHeadersPtrs[currentChunk] = (int*)data;

        // Здесь обращение к памяти не блокируем, т.к.обращение
        // идет при инициализации, она блокируется глобальным
        // системным семафором и все происходит последовательно
        if(created)
        {
            *(chunksHeadersPtrs[currentChunk]) = 0;
        }
        data += (int)Parameters::HEADER_SIZE;
        chunksDataPtrs[currentChunk] = data;
        data += dataChunksSizes[key];
    }

    this->chunksBytesAvailable[key] = chunksHeadersPtrs;
    this->chunksDataPtrs[key] = chunksDataPtrs;
}

QList<QString> Storage::storagesKeys() const
{
    return sharedStorages.keys();
}

int Storage::storageChunksCount(const QString &key) const
{
    const QString &lowerCaseKey = key.toLower();
    if(!dataChunksCounts.contains(lowerCaseKey))
    {
        qDebug() << "Storage: " << key << " not found";
        return -1;
    }

    return dataChunksCounts[lowerCaseKey];
}

int Storage::storageUsedChunksCount(const QString &key)
{
    const QString &lowerCaseKey = key.toLower();
    if(!sharedStorages.contains(lowerCaseKey))
    {
        StorageError::Code error = StorageError::Code::EC_KEY_NOT_AVAILABLE;
        const QString &errString = QString("Storage error: %1 : %2 - %3").arg(Q_FUNC_INFO).arg((int)error).
                                                                          arg(StorageError::toString(error));
        JournalSender::instance().sendEvent(UserIdentificator("User", -1), errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
        }

        return -1;
    }

    if(!sharedStorages[lowerCaseKey]->lock())
    {
        StorageError::Code error = StorageError::Code::EC_LOCK_ERROR;
        const QString &errString = QString("Storage error: %1 : %2 - %3").arg(Q_FUNC_INFO).arg((int)error).
                                                                          arg(StorageError::toString(error));
        JournalSender::instance().sendEvent(UserIdentificator("User", -1), errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
            printSharedMemoryError(sharedStorages[key]);
        }

        return -1;
    }

    int usedChunksCount = 0;
    for(int index = 0; index < dataChunksCounts[lowerCaseKey]; index++)
    {
        if((*chunksBytesAvailable[lowerCaseKey][index]) <= 0)
        {
            continue;
        }
        usedChunksCount++;
    }

    if(!sharedStorages[lowerCaseKey]->unlock())
    {
        StorageError::Code error = StorageError::Code::EC_UNLOCK_ERROR;
        const QString &errString = QString("Storage error: %1 : %2 - %3").arg(Q_FUNC_INFO).arg((int)error).
                                                                          arg(StorageError::toString(error));
        JournalSender::instance().sendEvent(UserIdentificator("User", -1), errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
            printSharedMemoryError(sharedStorages[lowerCaseKey]);
        }
    }

    return usedChunksCount;
}

int Storage::storageChunksSize(const QString &key) const
{
    const QString &lowerCaseKey = key.toLower();
    if(!dataChunksSizes.contains(lowerCaseKey))
    {
        qDebug() << "Storage: " << key << " not found";
        return -1;
    }

    return dataChunksSizes[lowerCaseKey];
}

int Storage::getNativeKey(const QString &key) const
{
    const QString &lowerCaseKey = key.toLower();
    if(!nativeKeys.contains(lowerCaseKey))
    {
        qDebug() << "Storage: " << key << " not found";
        return -1;
    }

    return nativeKeys[lowerCaseKey];
}

int Storage::storageCommonSize(const QString &key) const
{
    const QString &lowerCaseKey = key.toLower();
    if(!sharedStoragesSizes.contains(lowerCaseKey))
    {
        qDebug() << "Storage: " << key << " not found";
        return -1;
    }

    return sharedStoragesSizes[lowerCaseKey];
}

Storage::~Storage()
{
}

Storage &Storage::instance()
{
    static Storage instance;
    return instance;
}

DataKey Storage::insert(const QString &key, const QByteArray &data)
{
    const UserIdentificator &userId = UserIdentificator("User", -1);
    DataKey initialDataKey(key, -1, StorageError::Code::EC_UNDEFINED);
    JournalEventInfo eventInfo(initialDataKey, userId, messageTypeStorage, StorageOpCode::INSERT);
    JournalSender::instance().sendEvent(eventInfo, "Data inserting...");

    const QString &lowerCaseKey = key.toLower();
    if(!sharedStorages.contains(lowerCaseKey))
    {
        DataKey dataKey(key, -1, StorageError::Code::EC_KEY_NOT_AVAILABLE);
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::INSERT);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
        }

        return dataKey;
    }

    if(data.size() > dataChunksSizes[lowerCaseKey])
    {
        DataKey dataKey(key, -1, StorageError::Code::EC_DATA_SIZE_TOO_BIG);
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::INSERT);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
        }

        return dataKey;
    }

    if(!sharedStorages[lowerCaseKey]->lock())
    {
        DataKey dataKey(key, -1, StorageError::Code::EC_LOCK_ERROR);
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::INSERT);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
            printSharedMemoryError(sharedStorages[key]);
        }

        return dataKey;
    }

    StorageError::Code error = StorageError::Code::EC_NO_FREE_SPACE;
    int index = 0;
    quint64 operationTime = 0;
    for(index = 0; index < dataChunksCounts[lowerCaseKey]; index++)
    {
        if((*chunksBytesAvailable[lowerCaseKey][index]) <= 0)
        {
            memcpy(chunksDataPtrs[lowerCaseKey][index], data.data(), data.size());
            (*chunksBytesAvailable[lowerCaseKey][index]) = data.size();
            operationTime = Utils::getNanoSecsSinceEpoch();
            error = StorageError::Code::EC_OK;
            break;
        }
    }

    if(!sharedStorages[lowerCaseKey]->unlock())
    {
        DataKey dataKey(key, -1, StorageError::Code::EC_UNLOCK_ERROR);
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::INSERT);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
            printSharedMemoryError(sharedStorages[lowerCaseKey]);
        }

        return dataKey;
    }

    DataKey dataKey(key, index, error);
    postEvent(StorageOpCode::INSERT, operationTime, dataKey, data);

    JournalEventInfo journalEventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::INSERT);
    JournalSender::instance().sendEvent(journalEventInfo, "Data insert OK...");
    return dataKey;
}

QList<QPair<DataKey, QByteArray>> Storage::select(const QString &key, const QList<int> &indexes)
{
    const UserIdentificator &userId = UserIdentificator("User", -1);
    DataKey initialDataKey(key, -1, StorageError::Code::EC_UNDEFINED);
    JournalEventInfo eventInfo(initialDataKey, userId, messageTypeStorage, StorageOpCode::SELECT);
    JournalSender::instance().sendEvent(eventInfo, "Data selecting...");

    const QString &lowerCaseKey = key.toLower();
    if(!sharedStorages.contains(lowerCaseKey))
    {
        DataKey dataKey(key, -1, StorageError::Code::EC_KEY_NOT_AVAILABLE);
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::SELECT);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
        }

        return QList<QPair<DataKey, QByteArray>>() << qMakePair(dataKey, QByteArray());
    }

    if(!sharedStorages[lowerCaseKey]->lock())
    {
        DataKey dataKey(key, -1, StorageError::Code::EC_LOCK_ERROR);
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::SELECT);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
            printSharedMemoryError(sharedStorages[lowerCaseKey]);
        }

        return QList<QPair<DataKey, QByteArray>>() << qMakePair(dataKey, QByteArray());
    }

    QList<QPair<DataKey, QByteArray>> selectedData;
    QList<quint64> operationsTimesList;
    for(int index: indexes)
    {
        if(index < 0)
        {
            StorageError::Code error = StorageError::Code::EC_INDEX_IS_NEGATIVE;
            selectedData << qMakePair(DataKey(key, index, error), QByteArray());
            continue;
        }

        if(index >= chunksBytesAvailable[lowerCaseKey].size())
        {
            StorageError::Code error = StorageError::Code::EC_INDEX_OUT_OF_RANGE;
            selectedData << qMakePair(DataKey(key, index, error), QByteArray());
            continue;
        }

        int chunkDataSize = (*chunksBytesAvailable[lowerCaseKey][index]);
        if(chunkDataSize <= 0)
        {
            StorageError::Code error = StorageError::Code::EC_DATA_NOT_AVAILABLE_IN_INDEX;
            selectedData << qMakePair(DataKey(key, index, error), QByteArray());
            continue;
        }

        if(chunkDataSize > dataChunksSizes[lowerCaseKey])
        {
            StorageError::Code error = StorageError::Code::EC_INVALID_CHUNK_DATA_SIZE;
            selectedData << qMakePair(DataKey(key, index, error), QByteArray());
            continue;
        }

        QByteArray data(chunksDataPtrs[lowerCaseKey][index], chunkDataSize);
        operationsTimesList << Utils::getNanoSecsSinceEpoch();
        StorageError::Code error = StorageError::Code::EC_OK;

        selectedData << qMakePair(DataKey(key, index, error), data);
    }

    if(!sharedStorages[lowerCaseKey]->unlock())
    {
        DataKey dataKey(key, -1, StorageError::Code::EC_UNLOCK_ERROR);
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::SELECT);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
            printSharedMemoryError(sharedStorages[lowerCaseKey]);
        }

        selectedData << qMakePair(dataKey, QByteArray());
    }

    //postEvents(StorageOpCode::SELECT, operationsTimesList, selectedData);
    DataKey dataKey(key, -1, StorageError::Code::EC_OK);
    JournalEventInfo journalEventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::SELECT);
    JournalSender::instance().sendEvent(journalEventInfo, "Data select OK...");
    return selectedData;
}

QList<QPair<DataKey, QByteArray>> Storage::selectAll(const QString &key)
{
    const UserIdentificator &userId = UserIdentificator("User", -1);
    DataKey initialDataKey(key, -1, StorageError::Code::EC_UNDEFINED);
    JournalEventInfo eventInfo(initialDataKey, userId, messageTypeStorage, StorageOpCode::SELECT_ALL);
    JournalSender::instance().sendEvent(eventInfo, "Data selecting all...");

    const QString &lowerCaseKey = key.toLower();
    if(!sharedStorages.contains(lowerCaseKey))
    {
        DataKey dataKey(key, -1, StorageError::Code::EC_KEY_NOT_AVAILABLE);
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::SELECT_ALL);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
        }

        return QList<QPair<DataKey, QByteArray>>() << qMakePair(dataKey, QByteArray());
    }

    if(!sharedStorages[lowerCaseKey]->lock())
    {
        DataKey dataKey(key, -1, StorageError::Code::EC_LOCK_ERROR);
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::SELECT_ALL);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
            printSharedMemoryError(sharedStorages[lowerCaseKey]);
        }

        return QList<QPair<DataKey, QByteArray>>() << qMakePair(dataKey, QByteArray());
    }

    QList<QPair<DataKey, QByteArray>> selectedData;
    QList<quint64> operationsTimesList;
    for(int index = 0; index < dataChunksCounts[lowerCaseKey]; index++)
    {
        int chunkDataSize = (*chunksBytesAvailable[lowerCaseKey][index]);
        if((chunkDataSize <= 0) || (chunkDataSize > dataChunksSizes[lowerCaseKey]))
        {
            continue;
        }

        QByteArray data(chunksDataPtrs[lowerCaseKey][index], chunkDataSize);
        operationsTimesList << Utils::getNanoSecsSinceEpoch();
        StorageError::Code error = StorageError::Code::EC_OK;

        selectedData << qMakePair(DataKey(key, index, error), data);
    }

    if(!sharedStorages[lowerCaseKey]->unlock())
    {
        DataKey dataKey(key, -1, StorageError::Code::EC_UNLOCK_ERROR);
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::SELECT_ALL);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
            printSharedMemoryError(sharedStorages[lowerCaseKey]);
        }

        selectedData << qMakePair(dataKey, QByteArray());
    }

    //postEvents(StorageOpCode::SELECT_ALL, operationsTimesList, selectedData);

    DataKey dataKey(key, -1, StorageError::Code::EC_OK);
    JournalEventInfo journalEventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::SELECT_ALL);
    JournalSender::instance().sendEvent(journalEventInfo, "Data select all OK...");
    return selectedData;
}

QPair<DataKey, QByteArray> Storage::select(const QString &key, int index)
{
    const UserIdentificator &userId = UserIdentificator("User", -1);
    DataKey initialDataKey(key, index, StorageError::Code::EC_UNDEFINED);
    JournalEventInfo eventInfo(initialDataKey, userId, messageTypeStorage, StorageOpCode::SELECT);
    JournalSender::instance().sendEvent(eventInfo, "Data selecting...");

    const QString &lowerCaseKey = key.toLower();
    if(!sharedStorages.contains(lowerCaseKey))
    {
        DataKey dataKey(key, -1, StorageError::Code::EC_KEY_NOT_AVAILABLE);
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::SELECT);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
        }

        return qMakePair(dataKey, QByteArray());
    }

    if(index < 0)
    {
        DataKey dataKey(key, index, StorageError::Code::EC_INDEX_IS_NEGATIVE);
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::SELECT);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
        }

        return qMakePair(dataKey, QByteArray());
    }

    if(index >= chunksBytesAvailable[lowerCaseKey].size())
    {
        DataKey dataKey(key, index, StorageError::Code::EC_INDEX_OUT_OF_RANGE);
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::SELECT);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
        }

        return qMakePair(dataKey, QByteArray());
    }

    //quint64 operationTime = 0;
    if(!sharedStorages[lowerCaseKey]->lock())
    {
        DataKey dataKey(key, -1, StorageError::Code::EC_LOCK_ERROR);
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::SELECT);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
            printSharedMemoryError(sharedStorages[lowerCaseKey]);
        }

        return qMakePair(dataKey, QByteArray());
    }

    int chunkDataSize = (*chunksBytesAvailable[lowerCaseKey][index]);
    if(chunkDataSize <= 0)
    {
        DataKey dataKey(key, index,
                        StorageError::Code::EC_DATA_NOT_AVAILABLE_IN_INDEX);
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::SELECT);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
        }

        sharedStorages[lowerCaseKey]->unlock();
        return qMakePair(dataKey, QByteArray());
    }

    if(chunkDataSize > dataChunksSizes[lowerCaseKey])
    {
        DataKey dataKey(key, index,
                        StorageError::Code::EC_INVALID_CHUNK_DATA_SIZE);
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::SELECT);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
        }

        sharedStorages[lowerCaseKey]->unlock();
        return qMakePair(dataKey, QByteArray());
    }

    QByteArray data(chunksDataPtrs[lowerCaseKey][index], chunkDataSize);
    //operationTime = Utils::getNanoSecsSinceEpoch();
    StorageError::Code error = StorageError::Code::EC_OK;
    if(!sharedStorages[lowerCaseKey]->unlock())
    {
        error = StorageError::Code::EC_UNLOCK_ERROR;
        DataKey dataKey(key, index, error);
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::SELECT);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
            printSharedMemoryError(sharedStorages[lowerCaseKey]);
        }
    }

    //postEvent(StorageOpCode::SELECT, operationTime, dataKey, data);

    DataKey dataKey(key, index, error);
    JournalEventInfo journalEventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::SELECT);
    JournalSender::instance().sendEvent(journalEventInfo, "Data select OK...");
    return qMakePair(dataKey, data);
}

QPair<DataKey, QByteArray> Storage::select(const DataKey &dataIndex)
{
    return select(dataIndex.shmKey, dataIndex.index);
}

StorageError::Code Storage::update(const DataKey &dataIndex, const QByteArray &data)
{
    const UserIdentificator &userId = UserIdentificator("User", -1);
    JournalEventInfo eventInfo(dataIndex, userId, messageTypeStorage, StorageOpCode::UPDATE);
    JournalSender::instance().sendEvent(eventInfo, "Data updating...");

    const QString &lowerCaseKey = dataIndex.shmKey.toLower();
    if(!sharedStorages.contains(lowerCaseKey))
    {
        DataKey dataKey(dataIndex);
        dataKey.errorCode = StorageError::Code::EC_KEY_NOT_AVAILABLE;
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::UPDATE);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
        }

        return dataKey.errorCode;
    }

    if(dataIndex.index < 0)
    {
        DataKey dataKey(dataIndex);
        dataKey.errorCode = StorageError::Code::EC_INDEX_IS_NEGATIVE;
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::UPDATE);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
        }

        return dataKey.errorCode;
    }

    if(dataIndex.index >= chunksBytesAvailable[lowerCaseKey].size())
    {
        DataKey dataKey(dataIndex);
        dataKey.errorCode = StorageError::Code::EC_INDEX_OUT_OF_RANGE;
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::UPDATE);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
        }

        return dataKey.errorCode;
    }

    if(data.size() > dataChunksSizes[lowerCaseKey])
    {
        DataKey dataKey(dataIndex);
        dataKey.errorCode = StorageError::Code::EC_INVALID_CHUNK_DATA_SIZE;
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::UPDATE);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
        }

        return dataKey.errorCode;
    }

    quint64 operationTime = 0;
    if(!sharedStorages[lowerCaseKey]->lock())
    {
        DataKey dataKey(dataIndex);
        dataKey.errorCode = StorageError::Code::EC_LOCK_ERROR;
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::UPDATE);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
            printSharedMemoryError(sharedStorages[lowerCaseKey]);
        }

        return dataKey.errorCode;
    }

    memcpy(chunksDataPtrs[lowerCaseKey][dataIndex.index], data.data(), data.size());
    (*chunksBytesAvailable[lowerCaseKey][dataIndex.index]) = data.size();
    operationTime = Utils::getNanoSecsSinceEpoch();
    if(!sharedStorages[lowerCaseKey]->unlock())
    {
        DataKey dataKey(dataIndex);
        dataKey.errorCode = StorageError::Code::EC_UNLOCK_ERROR;
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::UPDATE);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
            printSharedMemoryError(sharedStorages[lowerCaseKey]);
        }
    }

    DataKey dataKey(dataIndex.shmKey, dataIndex.index, StorageError::Code::EC_OK);
    postEvent(StorageOpCode::UPDATE, operationTime, dataKey, data);
    JournalEventInfo journalEventInfo(dataKey, userId, messageTypeStorage,
                                      StorageOpCode::UPDATE);
    JournalSender::instance().sendEvent(journalEventInfo, "Data update OK...");
    return StorageError::Code::EC_OK;
}

StorageError::Code Storage::remove(const DataKey &dataIndex)
{
    const UserIdentificator &userId = UserIdentificator("User", -1);
    JournalEventInfo eventInfo(dataIndex, userId, messageTypeStorage,
                               StorageOpCode::REMOVE);
    JournalSender::instance().sendEvent(eventInfo, "Data removing...");

    const QString &lowerCaseKey = dataIndex.shmKey.toLower();
    if(!sharedStorages.contains(lowerCaseKey))
    {
        DataKey dataKey(dataIndex);
        dataKey.errorCode = StorageError::Code::EC_KEY_NOT_AVAILABLE;
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::UPDATE);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
        }

        return dataKey.errorCode;
    }

    if(dataIndex.index < 0)
    {
        DataKey dataKey(dataIndex);
        dataKey.errorCode = StorageError::Code::EC_INDEX_IS_NEGATIVE;
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::UPDATE);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
        }

        return dataKey.errorCode;
    }

    if(dataIndex.index >= chunksBytesAvailable[lowerCaseKey].size())
    {
        DataKey dataKey(dataIndex);
        dataKey.errorCode = StorageError::Code::EC_INDEX_OUT_OF_RANGE;
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::UPDATE);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
        }

        return dataKey.errorCode;
    }

    QByteArray data;
    quint64 operationTime = 0;
    if(!sharedStorages[lowerCaseKey]->lock())
    {
        DataKey dataKey(dataIndex);
        dataKey.errorCode = StorageError::Code::EC_LOCK_ERROR;
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::UPDATE);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
            printSharedMemoryError(sharedStorages[lowerCaseKey]);
        }

        return dataKey.errorCode;
    }

    int dataize = (*chunksBytesAvailable[lowerCaseKey][dataIndex.index]);
    if(dataize <= 0)
    {
        sharedStorages[lowerCaseKey]->unlock();

        DataKey dataKey(dataIndex);
        dataKey.errorCode = StorageError::Code::EC_DELETED;
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::UPDATE);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
        }

        return dataKey.errorCode;
    }

    data = QByteArray(chunksDataPtrs[lowerCaseKey][dataIndex.index], dataize);
    (*chunksBytesAvailable[lowerCaseKey][dataIndex.index]) = 0;
    operationTime = Utils::getNanoSecsSinceEpoch();

    if(!sharedStorages[lowerCaseKey]->unlock())
    {
        DataKey dataKey(dataIndex);
        dataKey.errorCode = StorageError::Code::EC_UNLOCK_ERROR;
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage, StorageOpCode::UPDATE);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
            printSharedMemoryError(sharedStorages[lowerCaseKey]);
        }

        return dataKey.errorCode;
    }

    DataKey dataKey(dataIndex.shmKey, dataIndex.index, StorageError::Code::EC_OK);
    postEvent(StorageOpCode::REMOVE, operationTime, dataKey, data);

    JournalEventInfo journalEventInfo(dataKey, userId, messageTypeStorage,
                                      StorageOpCode::REMOVE);
    JournalSender::instance().sendEvent(journalEventInfo, "Data remove OK...");
    return StorageError::Code::EC_OK;
}

StorageError::Code Storage::removeAll(const QString &key)
{
    const UserIdentificator &userId = UserIdentificator("User", -1);
    DataKey initialDataKey(key, -1, StorageError::Code::EC_UNDEFINED);
    JournalEventInfo eventInfo(initialDataKey, userId, messageTypeStorage,
                               StorageOpCode::REMOVE_ALL);
    JournalSender::instance().sendEvent(eventInfo, "Data removing all...");

    const QString &lowerCaseKey = key.toLower();
    if(!sharedStorages.contains(lowerCaseKey))
    {
        DataKey dataKey(key, -1, StorageError::Code::EC_KEY_NOT_AVAILABLE);
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage,
                                   StorageOpCode::REMOVE_ALL);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
        }

        return dataKey.errorCode;
    }

    if(!sharedStorages[lowerCaseKey]->lock())
    {
        DataKey dataKey(key, -1, StorageError::Code::EC_LOCK_ERROR);
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage,
                                   StorageOpCode::REMOVE_ALL);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
            printSharedMemoryError(sharedStorages[lowerCaseKey]);
        }

        return dataKey.errorCode;
    }

    QByteArray data;
    QList<QPair<DataKey, QByteArray>> removedData;
    QList<quint64> operationsTimesList;
    for(int index = 0; index < dataChunksCounts[lowerCaseKey]; index++)
    {
        int dataize = (*chunksBytesAvailable[lowerCaseKey][index]);
        if(dataize <= 0)
        {
            continue;
        }

        data = QByteArray(chunksDataPtrs[lowerCaseKey][index], dataize);
        (*chunksBytesAvailable[lowerCaseKey][index]) = 0;
        operationsTimesList << Utils::getNanoSecsSinceEpoch();
        StorageError::Code error = StorageError::Code::EC_OK;

        removedData << qMakePair(DataKey(key, index, error), data);
    }

    if(!sharedStorages[lowerCaseKey]->unlock())
    {
        DataKey dataKey(key, -1, StorageError::Code::EC_UNLOCK_ERROR);
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo eventInfo(dataKey, userId, messageTypeStorage,
                                   StorageOpCode::REMOVE_ALL);
        JournalSender::instance().sendEvent(eventInfo, errString);

        if(displayErrorsInfo)
        {
            qDebug() << errString.toLatin1().constData();
            printSharedMemoryError(sharedStorages[lowerCaseKey]);
        }

        removedData << qMakePair(dataKey, QByteArray());
    }

    postEvents(StorageOpCode::REMOVE_ALL, operationsTimesList, removedData);

    DataKey dataKey(key, -1, StorageError::Code::EC_OK);
    JournalEventInfo journalEventInfo(dataKey, userId, messageTypeStorage,
                                      StorageOpCode::REMOVE_ALL);
    JournalSender::instance().sendEvent(journalEventInfo, "Data remove all OK...");

    return StorageError::Code::EC_OK;
}

quint16 Storage::getNotificationPort() const
{
    return notificationPort;
}

quint16 Storage::loadPort(const QString &portKey, const QString &portName)
{
    JournalSender::instance().sendEvent("Notification port reading...", "Storage");
    const QList<QString> &keys = StorageSettings::Settings::instance(confFileName).
                                        groupKeys(SettingsParams::NETWORK_GROUP);
    QString journalPortString;
    bool containsJournalPort = false;

    for(const QString &key: keys)
    {
        if(key.toLower() == portKey.toLower())
        {
            containsJournalPort = true;
            journalPortString = key;
            break;
        }
    }

    if(!containsJournalPort)
    {
        const QString &initPortMsg = QString("%1 port key not found...").arg(portName);
        JournalSender::instance().sendEvent(initPortMsg, "Storage");
        qDebug() << portName << " port key not found...";
        return 0;
    }

    bool ok = false;
    quint16 port = StorageSettings::Settings::instance(confFileName).value(SettingsParams::NETWORK_GROUP + "/" +
                                                                           journalPortString).toUInt(&ok);
    if(!ok)
    {
        const QString &initPortMsg = QString("%1 port parsing error....").arg(portName);
        JournalSender::instance().sendEvent(initPortMsg, "Storage");
        qDebug() << portName << " port parsing error...";
        return 0;
    }

    const QString &initPortMsg = QString("Notification port reading OK. Port: %1").arg(port);
    JournalSender::instance().sendEvent(initPortMsg, "Storage");
    return port;
}

bool Storage::receiveOwnNotifications() const
{
    return receiveOwnEvents;
}

void Storage::setOwnNotificationsReception(bool receive)
{
    receiveOwnEvents = receive;
}

bool Storage::initSocket(quint16 port, QUdpSocket **socket, const QString &sockDestination)
{
    JournalSender::instance().sendEvent("Socket initialising...", "Storage");
    if(sockDestination.isEmpty())
    {
        const QString &errroString = QString("Socket destination not set; port: '%1'").arg(port);
        JournalSender::instance().sendEvent(errroString, "Storage");
        qDebug() << errroString;
        return false;
    }

    const QString &portString = QString("%1 port: %2").arg(sockDestination).arg(port);
    JournalSender::instance().sendEvent(portString, "Storage");
    qDebug() << portString.toLatin1().constData();

    QString socketString = QString("%1 socket").arg(sockDestination);
    (*socket) = new QUdpSocket();
    if(!(*socket))
    {
        socketString.append(" allocation ERROR...");
        JournalSender::instance().sendEvent(socketString, "Storage");
        qDebug() << socketString.toLatin1().constData();
        return false;
    }
    connect((*socket), &QUdpSocket::readyRead, this, &Storage::readPendingDatagrams);

    if(!(*socket)->bind(QHostAddress::Broadcast, port, QAbstractSocket::ReuseAddressHint))
    {
        socketString.append(" binding ERROR...");
        JournalSender::instance().sendEvent(socketString, "Storage");
        qDebug() << socketString.toLatin1().constData();
        printUdpSocketError((*socket));
        return false;
    }

    socketString.append(" initialisation OK...");
    JournalSender::instance().sendEvent(socketString, "Storage");
    qDebug() << socketString.toLatin1().constData();
    return true;
}

void Storage::readPendingDatagrams()
{
    while(notificationSocket->hasPendingDatagrams())
    {
        qint64 pendingDatagramSize = notificationSocket->pendingDatagramSize();
        const QNetworkDatagram &datagram = notificationSocket->receiveDatagram(pendingDatagramSize);
        uint32_t senderIp = datagram.senderAddress().toIPv4Address();

        uint16_t datagramSize = datagram.data().size();
        uint16_t payloadSize = datagramSize - sizeof(StorageEventInfo);
        StorageEventInfo eventInfo;
        memcpy(&eventInfo, datagram.data().constData(), sizeof(StorageEventInfo));
        if(payloadSize != eventInfo.payloadSize)
        {
            qDebug() << "Notification error: incorrect payload size";
            return;
        }

        if((eventInfo.pid == QCoreApplication::applicationPid()) &&
           (!receiveOwnEvents))
        {
            return;
        }

        eventInfo.setHostAddress(senderIp);

        emit eventReceived(StorageEvent(eventInfo, datagram.data().constData() + sizeof(StorageEventInfo)));
    }
}

StorageError::Code Storage::postEvents(StorageOpCode opCode, const QList<quint64> &operationTimes,
                                    const QList<QPair<DataKey, QByteArray>> &eventDataRecords) const
{
    const UserIdentificator &userId = UserIdentificator("User", -1);
    if(!notificationSocket)
    {
        StorageError::Code error = StorageError::Code::EC_UDP_SOCKET_NOT_INITIALISED;
        const QString &errString = QString("Storage 'postEvents()' error: notification socket not initialised (code: %1)...").
                                            arg((int)error);
        JournalSender::instance().sendEvent(userId, errString, messageTypeStorage);
        return error;
    }

    if(operationTimes.size() != eventDataRecords.size())
    {
        StorageError::Code error = StorageError::Code::EC_INVALID_CHUNK_DATA_SIZE;
        const QString &errString = QString("Storage 'postEvents()' error: invalid chunk data size...");
        JournalSender::instance().sendEvent(userId, errString, messageTypeStorage);
        return error;
    }

    int maxDataSize = getMaxDataSize(eventDataRecords);
    int headerSize = sizeof(StorageEventInfo);
    int bufferSize = headerSize + maxDataSize + 10;
    char buffer[bufferSize];

    StorageEventInfo *eventInfo = (StorageEventInfo*)buffer;
    char *dataPtr = buffer + headerSize;
    for(int currentRecotd = 0; currentRecotd < eventDataRecords.size(); currentRecotd++)
    {
        const QPair<DataKey, QByteArray> &eventData = eventDataRecords[currentRecotd];
        int commonDataLength = headerSize + eventData.second.size();
        eventInfo->operationCode = opCode;
        eventInfo->nanoSecsSinceEpoch = operationTimes[currentRecotd];
        const QString &appName = qApp->applicationName();
        memcpy(eventInfo->appName, appName.toLatin1().constData(), appName.size());
        eventInfo->appName[appName.size()] = '\0';
        memset(eventInfo->hostAddr, 0, StorageEventInfo::HOST_ADDR_SIZE);
        memcpy(eventInfo->shmKey, eventData.first.shmKey.toLatin1().constData(),
               eventData.first.shmKey.size());
        eventInfo->shmKey[eventData.first.shmKey.size()] = '\0';
        eventInfo->dataCellIndex = eventData.first.index;
        eventInfo->errorCode = eventData.first.errorCode;
        eventInfo->pid = QCoreApplication::applicationPid();
        eventInfo->payloadSize = eventData.second.size();

        memcpy(dataPtr, eventData.second.constData(), eventData.second.size());

        int dataSent = notificationSocket->writeDatagram(buffer, headerSize + eventData.second.size(),
                                                         QHostAddress::Broadcast, notificationPort);
        if(dataSent != commonDataLength)
        {
            StorageError::Code error = StorageError::Code::EC_INVALID_CHUNK_DATA_SIZE;
            const QString &errString = QString("Storage 'postEvents()' error: UDP notification send error (code: %1)...").
                                                arg((int)error);
            JournalSender::instance().sendEvent(userId, errString, messageTypeStorage);
        }
    }

    StorageError::Code error = StorageError::Code::EC_OK;
    const QString &errString = QString("Multiple notifications send OK (code: %1)...").
                                        arg((int)error);
    JournalSender::instance().sendEvent(userId, errString, messageTypeStorage);
    return error;
}

int Storage::getMaxDataSize(const QList<QPair<DataKey, QByteArray>> &records) const
{
    if(records.isEmpty())
    {
        return 0;
    }

    int maxDataSize = records.first().second.size();
    for(const QPair<DataKey, QByteArray> &record: records)
    {
        if(record.second.size() > maxDataSize)
        {
            maxDataSize = record.second.size();
        }
    }

    return maxDataSize;
}

StorageError::Code Storage::postEvent(StorageOpCode opCode, quint64 operationTime,
                                           const DataKey &dataKey, const QByteArray &data) const
{
    const UserIdentificator &userId = UserIdentificator("User", -1);
    if(!notificationSocket)
    {
        StorageError::Code error = StorageError::Code::EC_UDP_SOCKET_NOT_INITIALISED;
        DataKey errorKey(dataKey.shmKey, dataKey.index, error);
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo journalEventInfo(dataKey, userId, messageTypeStorage, opCode);
        JournalSender::instance().sendEvent(journalEventInfo, errString);
        return error;
    }

    int headerSize = sizeof(StorageEventInfo);
    int commonDataLength = headerSize + data.size();
    int bufferSize = commonDataLength + 10;
    char buffer[bufferSize];

    StorageEventInfo *eventInfo = (StorageEventInfo*)buffer;
    char *dataPtr = buffer + headerSize;

    eventInfo->operationCode = opCode;
    eventInfo->nanoSecsSinceEpoch = operationTime;
    eventInfo->setAppName(qApp->applicationName());
    eventInfo->setHostAddress(0);
    eventInfo->setShmKey(dataKey.shmKey);
    eventInfo->dataCellIndex = dataKey.index;
    eventInfo->errorCode = dataKey.errorCode;
    eventInfo->pid = QCoreApplication::applicationPid();
    eventInfo->payloadSize = data.size();
    memcpy(dataPtr, data.constData(), data.size());

    int dataSent = notificationSocket->writeDatagram(buffer, commonDataLength,
                                                     QHostAddress::Broadcast, notificationPort);
    if(dataSent != commonDataLength)
    {
        StorageError::Code error = StorageError::Code::EC_UDP_NOTIFY_SEND_ERROR;
        DataKey errorKey(dataKey.shmKey, dataKey.index, error);
        const QString &errString = QString("Storage error: %1 : %2").arg(Q_FUNC_INFO, dataKey.toString());
        JournalEventInfo journalEventInfo(dataKey, userId, messageTypeStorage, opCode);
        JournalSender::instance().sendEvent(journalEventInfo, errString);
        return StorageError::Code::EC_UDP_NOTIFY_SEND_ERROR;
    }

    StorageError::Code error = StorageError::Code::EC_OK;
    DataKey errorKey(dataKey.shmKey, dataKey.index, error);
    const QString &errString = QString("Notification send OK: %1").arg(dataKey.toString());
    JournalEventInfo journalEventInfo(dataKey, userId, messageTypeStorage, opCode);
    JournalSender::instance().sendEvent(journalEventInfo, errString);

    return error;
}

QList<StorageDescriptor> Storage::getStoragesDescriptors()
{
    QList<StorageDescriptor> storages;
    const QList<QString> &groups = StorageSettings::Settings::instance(confFileName).baseGroups();
    for(const QString &group: groups)
    {
        if(!group.startsWith(SettingsParams::STORAGES_PREAMBLE, Qt::CaseInsensitive))
        {
            continue;
        }

        StorageDescriptor descriptor;
        const QList<QString> &params = StorageSettings::Settings::instance(confFileName).groupKeys(group);
        for(const QString &param: params)
        {
            if(param.toLower() == SettingsParams::STORAGE_DESCRIPTION.toLower())
            {
                descriptor.description = StorageSettings::Settings::instance(confFileName).
                                            value(group + "/" + param).toString();
            }

            if(param.toLower() == SettingsParams::STORAGE_NAME.toLower())
            {
                descriptor.name = StorageSettings::Settings::instance(confFileName).
                                            value(group + "/" + param).toString();
            }

            if(param.toLower() == SettingsParams::STORAGE_TYPE.toLower())
            {
                const QString &type = StorageSettings::Settings::instance(confFileName).
                                            value(group + "/" + param).toString();
                if(type.toLower() == SettingsParams::STORAGE_TYPE_FIXED.toLower())
                {
                    descriptor.type = StorageDescriptor::StorageType::FIXED;
                }
                else if(type.toLower() == SettingsParams::STORAGE_TYPE_SCALABLE.toLower())
                {
                    descriptor.type = StorageDescriptor::StorageType::SCALABLE;
                }
                else
                {
                    descriptor.type = StorageDescriptor::StorageType::UNDEFINED;
                }
            }

            if(param.toLower() == SettingsParams::STORAGE_SIZE.toLower())
            {
                bool ok;
                int size = StorageSettings::Settings::instance(confFileName).
                                value(group + "/" + param).toInt(&ok);
                if(!ok)
                {
                    descriptor.size = -1;
                }
                else
                {
                    descriptor.size = size;
                }
            }

            if(param.toLower() == SettingsParams::STORAGE_ROW_SIZE.toLower())
            {
                bool ok;
                int rowSize = StorageSettings::Settings::instance(confFileName).
                                    value(group + "/" + param).toInt(&ok);
                if(!ok)
                {
                    descriptor.rowSize = -1;
                }
                else
                {
                    descriptor.rowSize = rowSize;
                }
            }

            if(param.toLower() == SettingsParams::STORAGE_SEGMENT_ID.toLower())
            {
                bool ok;
                int segmentId = StorageSettings::Settings::instance(confFileName).
                                    value(group + "/" + param).toInt(&ok);
                if(!ok)
                {
                    descriptor.segmentId = -1;
                }
                else
                {
                    descriptor.segmentId = segmentId;
                }
            }
        }
        storages << descriptor;
    }

    return storages;
}

bool Storage::displayErrors() const
{
    return displayErrorsInfo;
}

void Storage::setErrorsDisplaying(bool display)
{
    displayErrorsInfo = display;
}
