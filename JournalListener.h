#ifndef JOURNAL_H
#define JOURNAL_H

#include <QObject>
#include <QUdpSocket>
#include "SEvent.h"

class JournalListener: public QObject
{
    Q_OBJECT

    public:
        static JournalListener& instance();
        quint16 getJournalPort() const;

    private:
        JournalListener();
        JournalListener(const JournalListener& object);
        JournalListener& operator=(const JournalListener&);
        ~JournalListener();

        quint16 loadJournalPort();
        QUdpSocket* initJournalSocket(quint16 journalPort);
        void printUdpSocketError(const QUdpSocket *socket);

        quint16 journalPort;
        QUdpSocket *journalSocket;

        static QString confFileName;

    private slots:
        void readPendingDatagrams();

    signals:
        void eventReceived(JournalEvent event);
        void eventReceived(QByteArray data);
};

#endif // JOURNAL_H
