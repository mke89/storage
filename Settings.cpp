#include "Settings.h"
#include <QDebug>
namespace StorageSettings
{

Settings::Settings():settings("Vniira", qApp->applicationName())
{
    createDefaultSettings();
}

Settings::Settings(QString confFileName):settings("Vniira", confFileName)
{
    createDefaultSettings();
}

Settings &Settings::instance(QString confFileName)
{
    static Settings instance(confFileName);
    return instance;
}

void Settings::createDefaultSettings()
{
    setDefaultValue(SettingsParams::NETWORK_GROUP + "/" + SettingsParams::NOTIFICATION_PORT,
                    SettingsParams::DEFAULT_NOTIFICATION_PORT);
    setDefaultValue(SettingsParams::NETWORK_GROUP + "/" + SettingsParams::JOURNAL_PORT,
                    SettingsParams::DEFAULT_JOURNAL_PORT);
}

QVariant Settings::value(const QString &key) const
{
    return instance().settings.value(key);
}

QVariant Settings::value(QString key, QVariant defaultValue) const
{
    return instance().settings.value(key, defaultValue);
}

void Settings::setValue(const QString &key, const QVariant &value)
{
    instance().settings.setValue(key, value);
}

bool Settings::contains(const QString &key) const
{
    return instance().settings.contains(key);
}

void Settings::clearToTests()
{
    settings.clear();
    createDefaultSettings();
}

void Settings::removeGroup(const QString &groupName)
{
    settings.beginGroup(groupName);
    settings.remove("");
    settings.endGroup();
}

void Settings::setDefaultValue(const QString &name, const QVariant &value)
{
    if(settings.contains(name))
    {
        return;
    }

    settings.setValue(name, value);
}

QList<QString> Settings::allKeys() const
{
    return settings.allKeys();
}

QList<QString> Settings::baseGroups() const
{
    const QList<QString> &keys = settings.childGroups();
    return keys;
}

QList<QString> Settings::groupKeys(const QString &group)
{
    QList<QString> keys;
    settings.beginGroup(group);
    keys = settings.childKeys();
    settings.endGroup();

    return keys;
}

}
