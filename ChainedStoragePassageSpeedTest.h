#ifndef CHAINEDSTORAGEPASSAGESPEEDTEST_H
#define CHAINEDSTORAGEPASSAGESPEEDTEST_H

#include <QSharedMemory>

void runChainedStoragePassageSpeedTest(uint32_t storageSizeKBytes, uint32_t payloadSize, int cyclesCount);

#pragma pack(push, 1)
struct ShMemHeader
{
    quint32 size;           // data(payload) size, behind header
    quint64 timestamp;      // time of last operation
    quint32 index;          // unic index of record
    quint8  flags;          // for flags (free segment, in_use, deleted )
    quint16 checkSum;       // Check sum
    uchar   reserverd[1];
};
#pragma pack(pop)

class ChainedStoragePassageSpeedTest
{
    enum Params: int
    {
        HEADER_SIZE         = sizeof(ShMemHeader),
        CHECKABLE_DATA_LEN  =   sizeof(ShMemHeader::size) +
                                sizeof(ShMemHeader::timestamp) +
                                sizeof(ShMemHeader::index) +
                                sizeof(ShMemHeader::flags),
    };
    public:
        ChainedStoragePassageSpeedTest(uint32_t sizeKBytes, uint32_t payloadSize);
        ~ChainedStoragePassageSpeedTest();
        uint32_t passThroughTheMemory();
        bool fillMemory();
        uint32_t getBlocksCount();
        uint32_t getHeaderSize();
        uint32_t getPayloadSize();
        uint32_t getBlockSize();

    private:
        quint16 checkSum(const char *data, uint len);

        QSharedMemory *sharedMemory;
        uint32_t sizeBytes;
        uint32_t blocksCount;
        uint32_t payloadSize;
        uint32_t blockSize;
};

#endif // CHAINEDSTORAGEPASSAGESPEEDTEST_H

uint32_t getBlockSize();
