isEmpty(STORAGE_STUCTS_INCLUDED) {
# include this as .PRI rather than as dynamic library, if you want string lliterals translated.
STORAGE_STUCTS_INCLUDED = 1

INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

HEADERS  += \
    $$PWD/flight_object.h \
        $$PWD/plancons.h \   \
        $$PWD/sharedworkplace.h \
        $$PWD/plan.h   \
        $$PWD/planstru.h   \
        $$PWD/helpers.h \

}
