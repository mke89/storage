#ifndef SHAREDWORKPLACE_H
#define SHAREDWORKPLACE_H

#include <QtCore>
#include "plancons.h"

#pragma pack(push, 1)
namespace MemoryStructs
{
    struct Workplace
    {
         Workplace()
        {
            memset(this, 0, sizeof(Workplace));
        }

        enum SectorType: uint16_t
        {
            TypeInvalid     = 0,
            TypeAcc         = 1,
            TypeApproach    = 2,
            TypeAkdp        = 4,
            TypeZone        = 8,
            TypeMilitary    = 16,
            TypeA           = 32,
            TypeM           = 64
        };


        SectorType  sectorType;     // Тип сектора, битовая маска SectorType
        int         minLevelMeters; // Высота работы сектора от метров
        int         maxLevelMeters; // Высота работы сектора до метров
        char        geometry[520];  // Геометрия сектора в текторых координатах (max 32 координаты через пробел)
    };
}
#pragma pack(pop)

#endif // SHAREDWORKPLACE_H
