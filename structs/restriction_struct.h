#ifndef RESTRICTION_STRUCT_H
#define RESTRICTION_STRUCT_H

#include <QString>
#include <QDateTime>
//#include <gpoint.h>

struct restriction_struct
{
    QString name;
    enum TYPE{
        CIRCLE = 3,
        LINE = 4,
        AREA = 5      // POLYGONE
    };
    int type;
    enum FLAGS
    {
        CREATED = 0,// созданное ограничение
        CANCELED = 1, // ограничения снятое пользователем
        ACTUAL = 2, // ограничения активируемое пользователем
        ACTUAL_AUTO = 3 // автоматически активируемое ограничение
    };

 /*   <group name="Restrictions" visible="true" zOrder="500">
        <default foreColor="#FFFF0000" backColor="#7FFFFFFF" lineWidth="1" fillStyle="solid" />
        <!-- Pivp >> Restr-->
        <style name="Actual(noauto)"    zOrderDelta="1" backColor="#30FF0000" fillStyle="diagCross" />   <!-- DarkRed -->
        <style name="Cancelled(noauto)" zOrderDelta="1" backColor="#30FFFF00" fillStyle="diagCross" />   <!-- Yellow -->
        <!-- AutoRestr >> Restr -->
        <style name="ReadyToStart(auto)" backColor="#30FFFF00" />   <!-- Yellow -->
        <style name="Actual(auto)"       backColor="#30FF0000" />   <!-- Dark Red -->
        <style name="BeforeEnd(auto)"    backColor="#10FF0000" />   <!-- Light Red -->
        <!-- Pivp >> Working Ard (WA) -->
        <style name="ArdActual(noauto)"    zOrderDelta="1" backColor="#5FFFA000" fillStyle="cross" />   <!-- Light Orange -->
        <style name="ArdCancelled(noauto)" zOrderDelta="1" backColor="#1FFFA000" fillStyle="cross" />   <!-- Dark Orange -->
        <!-- AutoPlan >> Working Ard (WA) -->
        <style name="ArdActual(auto)"    backColor="#5FFF8000" />   <!-- Light Orange -->
        <style name="ArdCancelled(auto)" backColor="#1FFF8000" />   <!-- Light Orange -->
        <!-- Pivp >> ConflictRestrWithPlan -->
        <style name="ConflictRestrWithPlan" foreColor="#FFFFFF00" backColor="#FFFFFF00" fillStyle="solid" lineWidth="5" zOrder="999" />
    </group>*/
    int flag;
    QDateTime fromDateTime;
    QDateTime toDateTime;
    QString heightMin;
    QString heightMax;
    QString coordinates;
    QString radius;
    QString widthLine;
};


//Q_DECLARE_METATYPE(GPoint)

#endif // RESTRICTION_STRUCT_H
