#pragma once

#include <QByteArray>
#include <QDataStream>
#include <QIODevice>

namespace StructHelpers
{
//ONLY FOR RAW DATA SAVING (WITH CONST SIZE STRUCT)//
    template<typename T>
    static T fromByteArray( const QByteArray &ba )
    {
        T targetStruct;
        QDataStream ds( ba );
        ds.readRawData( (char*)&targetStruct, sizeof(T) );

        return targetStruct;
    }

    template<typename T>
    static QByteArray toByteArray( const T &sourceStruct )
    {
        QByteArray ba;
        QDataStream ds( &ba, QIODevice::WriteOnly );
        ds.writeRawData( (char*)&sourceStruct, sizeof(T) );

        return ba;
    }

    //ex: FlightObject fo = StructHelpers::fromByteArray<FlightObject>(someByteArrayObj);

};

