#ifndef POPLAN_H
#define POPLAN_H


#include "plancons.h"

#include <QtGlobal>
#include <QDataStream>
#include <QByteArray>
#include <string.h>

namespace MemoryStructs
{
#pragma pack(push, 1)
 struct plan
 {
    plan()
    {
        memset( this , 0, sizeof(plan) );
    }

    char field3[LEN_F3 + 1];    /*type*/
    char fld7[LEN_FLD7 + 1];        /*reise number/id/fld7*/
    char code[LEN_CODE + 1];    /*squawk*/
    char field8[LEN_F8+1];        /*FlyRules*/
    char numac[LEN_APNUM+1];      /*plans?/apNum*/
    char type[LEN_TYPE + 1];    /*apType/fld9*/
    char turbulence[LEN_TURBULENCE+1];           /*9.3*/
    char field10[LEN_F10 + 1]; /*field10_1_Equipment+field10_2_EquipmentAbility*/
    char av[LEN_AD + 1];       /*fld13_1/AOD*/
    char etd[LEN_TIME + 1];    /*fld13_1/TOD*/
    char speed[LEN_SPEED+1];     /*field15_1_Speed*/
    char level[LEN_HEIGHT+1];    /*field15_2_Height*/
    char field15[LEN_F15 + 1]; /*field15/fieldRoute*/
    char an[LEN_AD + 1];       /*field16_1_AOA*/
    char eta[LEN_TIME + 1];    /*field16_2_FTime*/
    char ad1[LEN_AD + 1];      /*field16_3_ARes1*/
    char ad2[LEN_AD + 1];      /*field16_4_ARes2*/
    char field18[LEN_F18 + 1]; /*field18_OtherInfo*/

    char pin[LEN_INOUTPOINT + 1];
    qint64 pinTime;
    char pinSpeed[LEN_SPEED + 1];
    char pinHeight[LEN_HEIGHT + 1];
    char pout[LEN_INOUTPOINT + 1];
    qint64 poutTime;
    char poutSpeed[LEN_SPEED + 1];
    char poutHeight[LEN_HEIGHT + 1];

    quint64 createdTimeStamp;
    uint planHashSum;
//    char sectors[LEN_SU+1];
//    char prev_code[LEN_CODE + 1];
//    char next_code[LEN_CODE + 1];
//    char assn_code[LEN_CODE + 1];
///    int error;                  /*errors in po*/
///    char text_error[100];       /*text of error*/
//    int num;                    // one-based list index
//    int num_fpl;                /*number of plan*/
//    int flag;                   /*po type:   0 - new plan,  1 - correction,  ...*/
//    int count;                  /*amount of plans*/
//    int flag_tlg;

//    char type_tlg[4];
//    char seria [3];
//    char addresses[LEN_ADDRESSEES+1];
//    char text[MAX_LEN_TLG+1];
//    int count_addresses;
//    int num_dbl;
//    int count_dbl;

//    int flag_dbl;

};
#pragma pack(pop)

}

#endif // POPLAN_H

