#ifndef PLANCONS_H
#define PLANCONS_H

// TYPE of AS UVD
#define ACC_TYPE      0  // ACC only (without TMA)
#define TMA_TYPE      1  // TMA only
#define TMA_ACC_TYPE  2  // TMA + ACC

#define SYS_TYPE TMA_TYPE

#define 	SEEK_SET 0
#define 	SEEK_CUR 1
#define 	SEEK_END 2

#define 	ONE_MILE 1.852 // [km]
#define 	RE	3442.*ONE_MILE // Radius of Earth
//#ifndef 	PI
//#define 	PI 3.1415926536
//#endif
#define 	FOOT_TO_METER_K (3048.0/10000.0)
#define 	METER_TO_FOOT_K (10000.0/3048.0)

#define 	MAXEL 100 // dimension of route_els[]

#define 	NUMBER_OF_POINTS 40 // dimension of pt[]
#define 	SPEED_MIN 50    // [km/h]
#define 	SPEED_MAX 4000  // [km/h]

#define 	MAXID 6


#define LEN_INOUTPOINT 5 /*длина поля имени точки входа/выхода (4 или 5 символов) */
#define LEN_TURBULENCE 1   /*9.3.1*/
#define LEN_SPEED 6     /*15.1*/
#define LEN_HEIGHT 5    /*15.2*/
#define LEN_APNUM 2     /* длина поля 9.1 кол-во ВC */
#define LEN_STR_TEXT 69 /* длина строки в форме "TEXT" */
#define LEN_F3 3        /* длина поля 3 плана */
#define LEN_F7 13       /* длина поля 7 плана */
#define LEN_FLD7 7        /* длина подполя номера рейса */
#define LEN_CODE 5      /* длина подполя кода И. АО */
#define LEN_F8 2        /* длина поля 8 плана */
#define LEN_F9 64       /* длина поля 9 плана */
#define LEN_TYPE 4      /* длина подполя типа самолета */
#define LEN_F10 128     /* длина поля 10 плана */
#define LEN_F13 256     /* длина поля 13 плана */
#define LEN_AD 4        /* длина имени аэродрома */
#define LEN_F15 2048    /* длина поля 15 плана */
#define LEN_F16 256     /* длина поля 16 плана */
#define LEN_F18 2048    /* длина поля 18 плана */
#define LEN_REG 7       /* длина подполя регистрационного номера */
#define LEN_CALL 7      /* длина подполя системного позывного */
#define LEN_MET 8       /* длина подполя метеоминимума */
#define LEN_STS 4       /* длина подполя литерности */
#define LEN_DATE 4      /* длина подполя даты */
#define LEN_FUEL 4      /* длина подлоля топлива */
#define LEN_MAIN 4      /* длина подполя основного аэродрома */
#define LEN_SGN_PNT 6   /* длина подполя точки или границы входа в зону */
#define LEN_EET 4
#define LEN_ADS_CODE 8  /* hex "FFFFFF" +2 reserv */
#define LEN_SECTORS 30
#define LEN_R7 7        /* длина поля 7 РПЛ */
#define LEN_R9 6        /* длина поля 9 РПЛ */
#define LEN_R15 53      /* длина поля 15 РПЛ */
#define RLEN_F15 54     /* длина поля 15 плана */
#define LEN_R16 8       /* длина поля 16 РПЛ */
#define RLEN_F16 9      /* длина поля 16 плана */
#define LEN_R18 53      /* длина поля 18 РПЛ */
#define RLEN_F18 54     /* длина поля 18 плана */
#define LEN_DAYS 7      /* длина поля дней выполнения рейса */
#define RLEN_DAYS 8     /* длина поля дней выполнения рейса */
#define LEN_VALID_FROM 6   /* длина поля начала действия расписания */
#define RLEN_VALID_FROM 7  /* длина поля дней вылета */
#define LEN_VALID_UNTIL 6  /* длина поля конца действия расписания */
#define LEN_ADDRESSEES 251  /* длина поля адресатов */
#define LEN_SPECADDRESS 32 /* длина поля спецадреса */
#define LEN_EXIT_BOUND 9   /* длина рубежa ухода */


#define     LEN_P_NAME	2
#define     LEN_P_FLAG  5
#define     LEN_SU 32
#define     LEN_f15_text_ms 72
#define     LEN_f18_text_ms 72
#define     LEN_SYS_NAME  10
#define     LEN_SYS_FLAG  4
#define     LEN_SYS_SMAR  64
#define     LEN_KEY_DOUBLE  15
#define     LEN_F7_FLAG 7
#define     LEN_F10_FLAG 2
#define     LEN_F9_FLAG 2
#define     LEN_F13_FLAG 7
#define     LEN_F15_FLAG 9
#define     LEN_F16_FLAG 9
#define     LEN_F18_FLAG 8
#define     LEN_ACP_NUMBER 2
#define     LEN_TIME 4
#define     LEN_FLAG_MODE 2
#define     LEN_FLAG_STATE 11
#define     LEN_RUNWAY 3
#define     LEN_SID 6
#define     LEN_NAM_PNT 6
#define     LEN_ROUTE_ELS 40
#define     LEN_OBOZN_EL 12
#define     LEN_F_FLAG 2
#define     LEN_F_NAME_PLT 2
#define     LEN_PHZ 4
#define     LEN_HTRS 5
#define     LEN_ORIG 10
#define     LEN_TM_ORIG 6
#define     LEN_MFLAG 4
#define     LEN_NPOZ 5
#define     LEN_SEND 15
#define     LEN_T_TEXT 200
#define     LEN_Q_KEY 4
#define     LEN_Q_FIELD 54
#define     LEN_COMMENT 30

#endif
