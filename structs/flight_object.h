#pragma once

#include "plancons.h"
#include "planstru.h"

#include <QtGlobal>
#include <QDataStream>
#include <QFlags>

#include <string.h>
#define LIST_LINE_LENGTH 111

/* структура элемента маршрута по плану */
typedef struct
{
    int x;
    int y;
    char reg[LEN_REG + 1];
    char time [6];
    char h[4];
} ROUTE_IVO;


/* структура строки для считывания списка входа в сектор */
typedef struct
{
    short num_ms;
    long itim;
    long t_confl;
    char ms_list[LIST_LINE_LENGTH]; /* inp_list_str ms_list; */
} input_list;


/* структура записи массива сопровождения ms бывший ControlArray*/
#pragma pack(push, 1)
struct FlightObject
{
    FlightObject()
    {
        memset(this, 0, sizeof(FlightObject));
    }


    enum MS_FLAG : uint32_t
    {
        none                = 0,
        activated           = (1 << 0), // have plan, by pult operation
        readyToTakeControl  = (1 << 1), // need to be taken on contol by operator
        bindedToTrack       = (1 << 2), // TODO control by pult operations
        manualPlanless      = (1 << 3), // сброс связи план-трек была через пультовую
        needGetControl      = (1 << 4), // need to get control
        needActivate        = (1 << 5), // need bind to plan
        onControl           = (1 << 6),
        isRVSM              = (1 << 7),

        // for sector transferring
        needTransfer        = (1 << 8),
        transferring        = (1 << 9),
        transferred         = (1 << 10),
        assumed             = (1 << 11),
        reqTransfer         = (1 << 12),
        reserve_4           = (1 << 13),
        reserve_5           = (1 << 14),
        reserve_6           = (1 << 15)
    };
    Q_DECLARE_FLAGS(MS_FLAGS, MS_FLAG)

    enum TRACK_FLAG : uint16_t
    {
        tf_none                     = 0,
        tf_have_survaliance         = (1 << 0), // трек идет, определяет planservice автоматом
        tf_have_survaliance_manual  = (1 << 1), // оператор назначил что трек идет, при этом, на самом деле трека может не быть
        tf_have_survaliance_plan    = (1 << 2),  // трек идет, в треке есть информация что он по плану
        tf_out_of_track_auto        = (1 << 3), // трек пропал, система обнаружила
        tf_out_of_track_manual      = (1 << 4), // трек пропал, оператор назначил
        tf_terminated_out_zone      = (1 << 5), // трек з0авершился по сообщению telminated за зоной управления
        tf_in_zone                  = (1 << 6), // трек в зоне управления АРМ или в зоне управления соседнего АРМ, расшифровка в какой именно зоне в отдельном текстовом поле с именем зоны
        tf_reserve_3                = (1 << 7),

        // for single sector identification
        tf_not_in_sector_yet        = (1 << 8),    // Трек еще не в секторе
        tf_in_sector                = (1 << 9),    // Трек в секторе
        tf_sector_leaved            = (1 << 10),   // Трек покинул сектор
        tf_reserve_4                = (1 << 11),
        tf_reserve_5                = (1 << 12),
        tf_reserve_6                = (1 << 13),
        tf_reserve_7                = (1 << 14),
        tf_reserve_8                = (1 << 15)
    };
    Q_DECLARE_FLAGS(TRACK_FLAGS, TRACK_FLAG)


    bool isLinkedWithPlan() const
    {
        return planHashSum > 0 ;
    }


    bool isLostTracking() const  //признак списка потерь flag_lost
    {
        return (trackFlags & tf_out_of_track_auto) || (trackFlags & tf_out_of_track_manual);
    }

    bool isPlanRVSM() const
    {
        return (flags & isRVSM);
    }


    bool isReadyToTakeControl()
    {
        return (flags & readyToTakeControl);
    }


    bool isPlanActivated() const
    {
        return (flags & activated);
    }

    void setPlanActivated(bool isActive = true)
    {
        if (isActive)
            flags |= activated;
        else
            flags &= ~activated;
    }


    bool isPlanBindedToTrack() const
    {
        return ( (track_number>0) &&
                 (flags & bindedToTrack) &&
                !(flags & manualPlanless) &&
                 (trackFlags & tf_have_survaliance) );
    }

    void bindPlanToTrack(uint32_t track_number, const QByteArray &actual_callsign,
                         uint32_t assigned_squawk, int afl)
    {
        this->track_number = track_number;
        memset(this->actual_callsign, 0, LEN_FLD7); //clear old data
        strncpy(this->actual_callsign, actual_callsign, LEN_FLD7);
        this->assigned_squawk = assigned_squawk;
        this->afl = afl;

        flags |= bindedToTrack;
        flags |= onControl;
        flags &= ~manualPlanless;
        trackFlags |= tf_have_survaliance;
    }

    void unbindPlanFromTrack()
    {
        this->track_number = 0;
        flags &= ~bindedToTrack;
        flags |= manualPlanless;
        trackFlags &= ~tf_have_survaliance;
    }


    /* массив, сформированный системой обработки радиолокационных данных */
    /* FROM_MRT rl; */
    /* массив, сформированный системой планирования */
    uint64_t createdTimeStamp; // nanoseconds; заполняется при создании
    uint64_t updateTimeStamp; // nanoseconds; заполняется при обновлении
    uint planHashSum; /* уникальный идентификатор плана вместо num_fpl*/
    MS_FLAGS flags;
    TRACK_FLAGS trackFlags;
    uint32_t track_number;
    uint32_t actual_squawk;
    char actual_callsign[LEN_FLD7+1]; /* номер рейса | nr | field_7 | id*/
    int afl; /* высота в метрах */


    uint32_t assigned_squawk;


    short num_vplt; /* номер взаимодействующего пульта */
    short num_pu; /* номер пульта управления */

    short num_stn; /* номер стоянки */

    int conflict_num_ms; /* номер конфликтующего по плану MS */
    /* поле M_ITIM1 в таблице MS */

    /* Выдерживание заданной высоты */
    /* поля M_ITIM2-M_ITIM3 в табл MS */
    char flag_hz_entered; /* признак задания высоты */
    char flag_hz_reached; /* признак достижения заданной высоты */
    char a_height_counter; /* счетчик сигнализации об отклонении от Нзад. */
    char a_height_ini_flag; /* flag of height values initialization */
    char A_vspeed_unit;
    char flag_skip;
    char flag_oldi; /* \0 - empty; \1 - ABI; \2 - ACT; */
    char flag_oldi_out; /* \0 - empty; \1 - ABI; \2 - ACT; \3 - REV */

    /* координаты точки начала конфликта по плану */
    int x_conflict; /* поле M_ITIM4 в табл MS */
    int y_conflict; /* поле M_ITIM5 в табл MS */

    long time_zap; /* время запуска или начала руления M_ITIM6 в табл MS */
    long fpl_int; /* дата вылета -  поле M_ITIM7 в табл MS */

    long time_mod; /* время коррекции MS */
    long time_del; /* время удаления MS */
    int first_conflict_time; /* время конфликта по плану */
    /* передача управления */
    short last_transfer; /* номер последнего пульта управления */
    char flag_handoff_transfer; /* признак ручной передачи */

    char blink_restrict; /* -------- */
    char rvsm;
    char freq833;

    short handoff_counter; /* счетчик полного ФС у передавшего диспетчера */

    /* параметры движения по программе */
    int X_prog; /* в км */
    int Y_prog; /* в км */
    int H_prog; /* в м */
    int V_prog; /* в км/час */
    int Curs_prog; /* в град. относительно севера по часовой стрелке */

    //  int hz;   /* заданная высота в 10м */
    //  int hp;   /* эшелон передачи управления в 10м */
    char hz[4]; /* заданная высота в 10м */
    char hp[4]; /* эшелон передачи управления в 10м */

    char used; /* признак записи, помеченной на удаление */

    /* поле M_FLAG длиной 7 байт */
    char trek; /* признак трека по плану - 'y' */
    char flag_drag; /* признак буксировки */
    char flag_blink; /* = 1 - признак входа в АДЦ (РЦ) */
    char flag_prog; /* признак движения по программе - 'y' */
    char flag_loss; /* признак списка : потерь - 'l', 'L' */
    char flag_wait; /* признак списка : зоны ожидания 'w', 'W'*/
    char flag_emrg; /* коды бедствия: 1-7500,2-7600,3-7700,4-2000 */
    /* 					НП,    РС,    БД,    КН */

    char reg [LEN_REG]; /* регистрационный номер */
    char code [LEN_CODE - 1]; /* код ИКАО */

    char flag_time [6]; /* признак введенного времени */
    /* 0 - признак изменения МС: 'y' - изм. маршрута, 'n' - нет изм.
    маршрута, 'c' - изм. поля конфликта, 'p' - изм. номера стоянки */
    /* 1 - признак retd */
    /* 2 - признак atd */
    /* 3 - признак eto */
    /* 4 - признак reto или ato */
    /* 5 - признаk ata */

    char ri [LEN_SU];
    char npoz[LEN_NPOZ]; /* новый позывной */
    char htrs[LEN_HTRS]; /* высота на трассе */

    char route_direction; /* признак направления полета
                                                   0-восточное, 1-западное*/
    char foreign; /* признак латинского регистра */
    char international; // признак принадлежности авиакомпании Государству (СНГ)
    char route_direct; /* признак спрямления маршрута */
    char flight_type; /* тип полета - military */
    char vfr; /* признак ПВП - 1, ИПП - 0 */
    char training; // признак принудительного распределения по секторам

    /* поле M_FLD9 длиной 7 байтов */
    char type [LEN_TYPE]; /* тип самолета */
    char weight; /* 'Л' - легкий, 'М' - средний, 'Х' - тяжелый */
    char f9_zzzz; /* признак "ЗЗЗЗ" */
    char secondary; /* признак наличия бортового ответчика ИКАО */

    /* поле M_FLD13 длиной 15 байтов */
    char av[LEN_AD]; /* аэродром вылета */
    char eobt[LEN_TIME]; /* время уборки колодок */
    char f13_sn[LEN_AD]; /* системное имя аэродрома вылета - 4 байта*/
    char f13_zzzz; /* признак "ЗЗЗЗ" */
    char f13_afil; /* признак "АФИЛ" */
    char f13_main; /* признак основного аэродрома */

    /* поле M_FLD16 длиной 13 байтов */
    char an[LEN_AD]; /* аэродром назначения */
    char f16_sn[LEN_AD]; /* системное имя аэродрома назначения - 4 байта*/
    char f16_zzzz; /* признак "ЗЗЗЗ" для аэродрома назначения*/
    char f16_zzzz_alt; /* признак "ЗЗЗЗ" для запасных аэродромов */
    char f16_main; /* признак основного аэродрома */
    char f16_alt_1; /* признак наличия 1-го запасного аэродрома */
    char f16_alt_2; /* признак наличия 2-го запасного аэродрома */

    /* поле M_FLD18 длиной 33 байтов */
    char call [LEN_CALL]; /* сменный позывной */
    char met [LEN_MET]; /* метеоминимум */
    char sts [LEN_STS]; /* литерность */
    char fuel [LEN_FUEL]; /* топливо */
    char main [LEN_MAIN]; /* основной аэродром */
    char sgn_pnt [LEN_SGN_PNT]; /* точка или граница входа в зону */

    /* поле M_FLD2 длиной 73 байтов */
    char acp_number [2]; /* номер категории летно-технических
                      характеристик (aircraft performance category number) */
    char A_speed_unit; /* asigned speed unit M or K or N */
    short taxi_time; /* время руления (в минутах) */
    char etd [4]; /* расчетное время вылета */
    char etdr [4]; /* расчетное время вылета по расписанию */
    char atd [4]; /* фактическое время вылета */
    char eto [4]; /* расчетное время входа в зону */
    char reto [4]; /* уточненное расчетное время входа в зону */
    char eta [4]; /* расчетное время прибытия */
    char reta [4]; /* уточненное расчетное время прибытия */
    char fl_mode; /* характер полета: 'В' - вылет, 'П' - прилет,
                                'Т' - транзит, 'С' - вылет с прилетом */
    char current_mode; /* текущий статус: 'В' - вылет,
                         'П' - прилет, 'Т' - транзит */
    char runway_dep [3]; /* ВПП взлета */
    char runway_arr [3]; /* ВПП посадки */
    char sid [7]; /* стандартный маршрут вылета */
    char star [7]; /* стандартный маршрут прибытия */
    char pnt_sid [LEN_NAM_PNT]; /* точка подключения СИД */
    char pnt_star [LEN_NAM_PNT]; /* точка подключения СТАР */
    char phz [4]; /* заданная высота (задана при активизации) */
    char fas; /* номер первого сектора подхода */
    char fcs; /* номер первого сектора управления */

    /* поле M_STNNAM длиной 4 байта */
    char name_stn[4]; /* имя стоянки */

    /* поле M_LAND длиной 4 байта */
    char land_sys[4]; /* способ захода на посадку */

    /* поле M_FLDACT длиной 20 байтов */
    char teet[4]; /* общее расчетное время полета в 16 поле */
    char act_text[16]; /* текст поля АСТ */

    /* поле M_TAX длиной 2 байта */
    char flag_tax; /* признак формирования сбора налогов */
    char flag_rcact; /* признак формирования РЦАЦТ */
    /* y - признак записи сообщения РЦAЦТ в очередь */
    /* r - признак отправления телеграммы типа РЦAЦТ в очередь */

    /* поле M_ISTR1 длиной 18 байтов */
    char name_sectrc[2]; /* имя сектора РЦ */
    char min_ks; /* признак соответствия метеоминимума */
    /* ' ' - соответсвие, b - несоответствие по
    боковому ветру, m - минимум КВС */
    char input_man; /* признак ручного оформуляривания */
    char CFL_unit; // S - 100m   F - 100F
    char TFL_unit; // S - 100m   F - 100F
    //#ifdef SPARC
    char change_cond;
    char flag_5num;
    //#endif
    int time; /* время для реж PLAYBACK от _sysdate()  */
    int date; /* дата  для реж PLAYBACK от _sysdate()  */
    int tick; /* тики  для реж PLAYBACK от _sysdate()  */

    char flag_fli;
    char flag_fli_out;

    char code_act[4];
    char code_ADS_B[LEN_ADS_CODE];

    char AManTime;   /* required time deviation (delay or speed up), +/-[min] */
    char responder_regime; // 1 - QNH, 2 - QFE, 0 - not defined
    char rezerv0 [64-LEN_SU];
    /* поле M_ISTR2 в таблице MS */
    /* поле M_ISTR3-M_ISTR7 длиной 5*48=240 байтов */
    int A_heading; /* asigned heading */
    int A_speed; /* asigned speed */
    int A_vspeed; /* asigned vertical speed */
    int deviation; /* Deviation from route in [meters] */
    int numac; /* количество ВС в группе */
    int source_parent; /* уникальный номер плана в АС АНО */
    char current_code[4]; /* текущий код ВРЛ; ABI or SFPL */
    char altn1[LEN_AD]; /* Запасной аэродром 1 */
    char altn2[LEN_AD]; /* Запасной аэродром 2 */
    char acode[4]; /* Код Авиакомпании */
    int act_out_time;
    char exit_level[4]; /* высота выхода - в последней точке с ед. измерения */
    char exit_pnt[5]; /* имя точки выхода из РЦ */
    char flag_pvo;
    char rezerv1[38];
    char entry_fir[4];
    char exit_fir[4];
    char stat_in[2];
    char stat_out[2];

    int time_msg_in;
    int time_msg_out;

    int time_oldi[12];
    char num_sect[12];
    char type_oldi[12];

    char rezerv2[56];

    /* поле M_F15T длиной 72 байта */
    char f15_text[72]; /* текст 15-го поля */

    /* поле M_F18T длиной 72 байта */
    char f18_text[72]; /* текст 18-го поля */

    /* Точки маршрута */
    struct point pt[NUMBER_OF_POINTS];

}; /* struct ms */
Q_DECLARE_OPERATORS_FOR_FLAGS(FlightObject::TRACK_FLAGS)
Q_DECLARE_OPERATORS_FOR_FLAGS(FlightObject::MS_FLAGS)
#pragma pack(pop)


