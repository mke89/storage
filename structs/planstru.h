#ifndef PLANSTRU_H
#define PLANSTRU_H

#include "plancons.h"

//------------------------------------------------------------
struct flight_parameters
{
  unsigned short rsp_km; // reqwested SPEED [km/h]
  unsigned short rfl_m;  // reqwested FL [m]
  unsigned short rsp;    // reqwested SPEED
  unsigned short rfl;    // reqwested FL
  char rsp_unit;        // rsp unit: 'K'[km/h] or 'N'[knots] or 'M' [0.01Mach]
  char rfl_unit;        // rfl unit: 'F'[foots] or 'S'[meters]
  char flight_rool;     // Flight rool: 'I' - instumental FR, 'V' - visual FR
  unsigned char ac_num; // AC quantity
  unsigned char glub;   // glubina boevogo porjadka [min]
  unsigned short diap;  // diapazon boevogo porjadka [m]
  char rpu_flag;        // /RPU/  flag
  char dct_flag;        // indicator DCT flag
};
typedef struct flight_parameters t_FlPar;
//------------------------------------------------------------
struct field3
{
    char text [LEN_F3];		/* 'ФПЛ'/'FPL', 'ППЛ'/'PPL', 'РПЛ'/'RPL'*/
    char type; 				/* 'Ф', 'П', или 'Р' */
};

struct field7
{
    char text [LEN_F7];		/* XXXXXXX[/A7777] */
    char nr [LEN_FLD7]; 		/* номер рейса (до 7-ми символов)*/
    char code [LEN_CODE]; 	/* код ИКАО (A7777)*/
    char route_direction; 	/* признак направления полета
                               0-восточное, 1-западное*/
    char foreign; 			/* признак латинского регистра */
    char change_cond; 	//SSR code change codition. (was - international)
    char delay; 			/* занят Кодиной для документирования */
    char out_of_schedule; 	/* резерв - признак рейса вне расписания */
    char vfr;		 		/* признак ПВП - 1, ИПП - 0 */
    char training; 			/* резерв - признак тренировочного полета по кругу */
};

struct field8
{
    char text [LEN_F8];		/* AA */
    char military; 			/* тип полета - ВС МО */
};

struct field9
{
    char text [LEN_F9];		/* [99]AXXX[/A] */
    char type [LEN_TYPE]; 	/* тип самолета - от 2-х до 4-х символов */
    char weight; 			/* 'Л' - легкий, 'М' - средний, 'Х' - тяжелый */
    char zzzz; 				/* признак "ЗЗЗЗ" */
    int numac;              /*чмсло самолетов в группе*/
};

struct field10
{
    char text [LEN_F10];	/* A[A...]/A */
    char z; 				/* признак "З" */
    char secondary; 		/* признак наличия бортового ответчика ИКАО */
    char rvsm;              /* признак уменьшеного эшелонирования*/
    char freq833;           /*признак частоты*/
};

struct field13
{
    char text [LEN_F13];	/* AAAA9999 */
    char sys_name[4]; 		/* системное имя аэродрома - AAAA */
    char zzzz; 				/* признак "ЗЗЗЗ" */
    char afil; 				/* признак "АФИЛ" */
    char main; 				/* признак основного аэродрома */
};

struct field15
{
    char text [LEN_F15];	/* текст описания маршрута полета */
    char crspeed[5]; /* крейсерская скорость on first route segment.
                        format: A999[9], where
                                 A - measure unit;
                                 999[9] - speed value.
                        measure unit: 'K' - [1km/h];
                                      'N' - [1knot];
                                      'M' - [0.01 Mach]
                     */
    char crlevel[5]; /* крейсерский эшелон on first route segment.
                        format: A999[9], where
                                 A - measure unit;
                                 999[9] - level value.
                        measure unit: 'F' or 'A' - [100f];
                                      'S' or 'M' - [10m].
                      */

//  30 Oct 2006    // Field format: A999.  Dimention [100f] or [100m]
    char FL_before_COPin[4];    // column "PEL" before COPin
    char FL_after_COPin[4];     // column "RFL" after COPin
    char FL_before_COPout[4];   // column "TFL" before COPout
    char FL_after_COPout[4];    // column "RFL" after COPout
//  30 Oct 2006

    struct
    {
        char name; 			/* имя несистемной точки */
        short f_offset;		/* смещение текста, описывающего точку в маршруте */
    }
    np_coord [MAXID-4]; 	/* таблица перевода координат для несистемных точек
                               MAXID=6
                             */
    short freacr_offset;   	/* смещение текста, описывающего первый
                        элемент маршрута после зоны управления, в маршруте */
};

struct field16
{
    char text [LEN_F16];	/* AAAA9999[ AAAA][ AAAA] */
    char sys_name[4]; 		/* системное имя аэродрома */
    char zzzz; 				/* признак "ЗЗЗЗ" для аэродрома назначения */
    char zzzz_alt; 			/* признак "ЗЗЗЗ" для запасных аэродромов */
    char main; 				/* признак основного аэродрома */
    char alt_1; 			/* признак наличия 1-го запасного аэродрома */
    char alt_2; 			/* признак наличия 2-го запасного аэродрома */
};

struct field18
{
    char text [LEN_F18];	/* текст поля */
    char reg [LEN_REG];
    char ads_code [LEN_ADS_CODE];
    char call [LEN_CALL];   /* сменный позывной - до 7 символов */
    char met [LEN_MET];     /* метеоминимум - до 7 символов (999 9,9)*/
    char sts [LEN_STS];     /* литерность - до 4 символов*/
    int mday;   			/* номер дня месяца (1 - 31) */
    int mon;    			/* номер месяца (0 - 11) */
    int dof;                /* дата выполнения рейса */
    char fuel [LEN_FUEL]; 	/* остаток топлива - 4 цифры (2400) */
    char main [LEN_MAIN]; 	/* основной аэродром - 4 буквы */
    char sgn_pnt [LEN_SGN_PNT]; /* точка или граница входа в зону */
    char eet [LEN_EET];
    char flag_eet;
    char flag_dep; 			/* признак наличия подполя DEP/ */
    char flag_5num; 		/* признак наличия кода ответчика УВД из 5 цифр */
    char flag_opr; 			/* признак наличия подполя OPR/ */
    char flag_typ; 			/* признак наличия подполя TYP/ */
    char flag_com;			/* признак наличия подполя COM/ */
    char flag_nav; 			/* признак наличия подполя NAV/ */
    char flag_dest; 		/* признак наличия подполя DEST/ */
    char flag_altn; 		/* признак наличия подполя ALTN/ */
};

struct point
{
    int time;		/* расчетное время пролета точки (сек) */
    float x;       	/* координата точки x (км) */
    float y;       	/* координата точки y (км) */
    short ch;      	/* расчетная высота пролета точки (м) */
    char sect;		/* номер сектора управления */
    char name [LEN_NAM_PNT];
    char type;
    short rfl;
    float lur;     	/* упреждение до начала разворота  (км) */
    float cv;      	/* расчетная скорость пролета точки */
    float dist;
    short v;       	/* заданная скорость в точке (км/ч) */
    short h;       	/* заданная высота в точке (м) */
    /* Следующие поля содержат признаки, необходимые для */
    /* распределения информации */
    char rp;       	/* пункт обязательного донесения */
    char act;      	/* пункт ввода сообщения АЦТ */
    char hld;      	/* опорный пункт зоны ожидания HOLD */
    char is;       	/* пункт входа в State => граница Государства */
    char os;       	/* пункт выхода из State */
    char enr;      /* пункт входа в AoR (Area of Responsibility) => граница АоР  */
    char exr;      	/* пункт выхода из AoR (Area of Responsibility) */
    char ent;      	/* пункт входа в TMA -аэроузел => граница ТМА */
    char ext;      	/* пункт выхода из TMA -аэроузла */
    char ens;      /* пункт входа в сектор => граница сектора */
    char exs;      /* пункт выхода из сектора */
/**
 * exs - признак принадлежности точки:
   0 - самостоятельная точка маршрута
   1 - принадлежит международной трассе
   2 -  принадлежит МВЛ
   3 -  принадлежит  ЗОНЕ
   4 -  принадлежит маршруту расхождения МПЛ
**/

    char enz;      	/* пункт входа в круг => граница КРУГА*/
    char exz;      	/* пункт выхода из круга */
    /**
     * exz - Pivp to ATD - тип линии для маршрута от 0
     *      0 - сплошная
     *      1 - нет линии
     *      2 - пунктирная
     * exz - Pivp to ATD - тип точки маршрута от 100
     *      100 - текущее расположение
    **/
    char ad;       	/* пункт - аэродром */
//    char dprm;     	/* пункт - дальний приводной радиомаяк (резерв) */
    char sid_star;  /* field "sid_star" insted of field "dprm" */
                    /* признак - point belonging to SID or STAR
                     - 0 - point is not belonging to SID/STAR;
                     - 1 - point belonging to SID;
                     - 2 - point belonging to STAR;
                     */

    /* Следующие поля содержат дополнительные признаки */
    char aato; 		/* признак ввода АТО ('\0' - ввода не было,
                        'А' - автоматически, 'Р' - вручную) */
    char eet; 		/* признак наличия в 18 поле времени пролета точки */
//    char prop; 		/* признак расчета в обратном порядке */
    char vert_man; /* признаки вертикального маневра в точке:
                   - 0 - нет вертикального маневра;
                   - 1 - начало маневра;
                   - 7 - окончание маневра;
                   */
    char hcr; 		/* признак крейсерского эшелона */
    char rfl_unit;	/* единицы измерения RFL - 'F'[foots] or 'S'[meters] */
};      /* sizeof (struct point)=60 */
    /*  При изменении скорректировать F_ROUTE и R_ROUTE */

typedef struct point st_point;

///////////////////////////////////////// 27 May 2011
typedef struct  {double x; double y;} t_point;
typedef t_point dt_point;

typedef struct  {t_point beg; t_point end;} t_segment;
typedef t_segment dt_segment;

typedef struct {t_point centre; double angle;} t_centre;

typedef struct  {char name [5];  t_point coord;} nxyPoint;

struct enroute_point_link {
  // entry info
  int info_type;  /*  entry info type:
            1 - point name only
                        2 - point coordinates only
            3 - point name and coordinates
          */
  int num;  // number of point in route_els[ ]
  int metod;    /* link metod:
                1 - by equel name or equel x,y
        2 - by x,y on route segment
        3 - by min_distance from point to route point
        4 - "direction"  for aerodromes offside AoR (DEP or DEST)
        5 - "connection"  for aerodromes inside AoR (DEP or DEST)
        */

  //  resultat
  int num1; // number  of  first enroute point in enroute_point [ ]
  int num2; // number  of  second enroute point  in enroute_point [ ]  (for enroute segment)
  char name1 [5];  // first enroute point name
  char name2 [5];  // second enroute point name (for enroute segment)
 };

typedef struct enroute_point_link EP_link;

///////////////////////////////////////// 27 May 2011

struct plan_pnt
{
    struct point rou_pnt[NUMBER_OF_POINTS];
        /* struct point pt[NUMBER_OF_POINTS] */
};      /* end struct  plan_pnt */

/** СТРУКТУРА ПЛАНА ПОЛЕТА **/
struct fpl
{
    int time_act;
    int time_del; /* время удаления плана/конец действия расписания для РПЛ*/
    int time_mod; /* время коррекции плана*/
    char act;          	/* признак активизации плана */
    char current_mode; 	/* текущий статус: 'D' - вылет,
                            'A' - прилет, 'T' - транзит */
    char nam_act [LEN_NAM_PNT]; /* имя точки активизации плана */
    char exit_pnt [5];     /* имя точки выхода из зоны */
    char exit_time [4];     /* время выхода из зоны */
    char exit_level [4];     /* высота выхода из зоны */
    char entry_level[4];     /* высота входа в 1-ой точке зоны - rfl */

    char entry_fir[4];
    char exit_fir[4];

    char current_code[4];  /* текущее значение кода ИКАО */
    int num;         	/* номер записи в файле fpl.dat */
  /* Текстовая часть плана и непосредственно определяемые из нее параметры */
    struct field3 f3;
    struct field7 f7;
    struct field8 f8;
    struct field9 f9;
    struct field10 f10;
    struct field13 f13;
    struct field16 f16;
    struct field15 f15;
    struct field18 f18;

    char acp_number [2]; /* (aircraft performance category number) */
    int taxi_time;     /* время руления (в минутах) */
    char etdr [4];       /* расчетное время вылета по расписанию */
    char etd [4];        /* расчетное время вылета */
    char atd [4];        /* фактическое время вылета */
    char eto [4];        /* расчетное время входа в зону */
    char reto [4];       /* уточненное расчетное время входа в зону */
    char eta [4];        /* расчетное время прибытия */
    char reta [4];       /* уточненное расчетное время прибытия */
    char fl_mode;        /* характер полета: 'D' - вылет, 'A' - прилет,
                         'T' - транзит, 'L' - локальный (вылет с прилетом) */
    char outside_rout; /* резерв - признак внетрассового полета */
    char flag_atd;     /* признак ввода фактического времени вылета */
    char change;       /* признак изменения маршрута */
    char circle; 
    char broken_route; /* признак разрывного маршрута */
    char manual_dep;   /* признак ручного ввода ВПП взлета */
    char manual_arr;   /* признак ручного ввода ВПП посадки */
    char manual_sid;   /* признак ручного ввода СИД */
    char manual_star;  /* признак ручного ввода СТАР */
    char compulsory;   /* признак принудительного распределения информации */
    char runway_dep [3]; /* ВПП взлета */
    char runway_arr [3]; /* ВПП посадки */
    char sid [7];      /* стандартный маршрут вылета */
    char star [7];     /* стандартный маршрут прибытия */
    short pnt_act;     /* номер (в маршруте) точки ввода сообщения АЦТ
                          (активизации плана) */
    char pnt_sid [LEN_NAM_PNT];  /* точка подключения СИД */
    char pnt_star [LEN_NAM_PNT]; /* точка подключения СТАР */
    char phz [4];  /* заданная высота (задана при активизации) */
    char ri [LEN_SU];
    char fas;      /* номер первого сектора подхода */
    char fcs;      /* номер первого сектора управления */
    char sectors [LEN_SECTORS]; /* сектора УВД (при принудительном
                              распределении информации) */
    char orig [10];      /* составитель плана */
    char orig_time [6];  /* число и время составления плана */
    char modif [10];     /* модификатор плана */
    char modif_time [6]; /* число и время последней модификации плана */

             /* Точки маршрута */
    struct point pt[NUMBER_OF_POINTS];

    char days [LEN_DAYS];             /* дни выполнения рейса по расписанию */
    char valid_from [LEN_VALID_FROM];   /* начало действия расписания */
    char valid_until [LEN_VALID_UNTIL]; /* конец действия расписания */

    unsigned char error[16];			/* массив ошибок в полях плана
                               [0] - Серия
                               [1] - Адрес
                               [2] - Спец адрес
                               [3] - Секторы УВД
                               [4] - Поле 3
                               [5] - Поле 7
                               [6] - Поле 8
                               [7] - Поле 9
                               [8] - Поле 10
                               [9] - Поле 13
                               [10] - Поле 15
                               [11] - Поле 16
                               [12] - Поле 18
                               [13] - Поле С
                               [14] - Поле ПО
                               [15] - Поле Срок действия */
};

/* Адресная часть телеграммы */
struct address_part
{
    char seria [2];                        /* серия срочности */
    char addressees [LEN_ADDRESSEES];      /* адресаты */
    char specaddress [LEN_SPECADDRESS];    /* специальное обозначение
                                            адресата и (или) отправителя */
};

struct points_menu 
{
    char name[LEN_NAM_PNT];	/* point's name. LEN_NAM_PNT=5+1; */
    char flag[2];	/* overflow flag: '*' - overflow; ' ' - not overflow; */
};

#endif
