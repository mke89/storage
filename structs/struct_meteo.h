#ifndef STRUCT_METEO_H
#define STRUCT_METEO_H

#include <QString>
#include <QHash>

struct Wind
{
    double angle;
    double speed;

    Wind()
    {
        angle = -1;
        speed = -1;
    }

    bool isEmpty()
    {
        return angle < 0 && speed < 0;
    }
};

struct ParamRw
{

    double  pressureLeft;
    double  pressureRight;

    enum  STATERUNWAY
    {
          CLOSE = 0,
          OPEN = 1
    };
    int     stateRunWay;

    enum COURSE
    {
        LEFT = 0,
        RIGHT = 1
    };
    int     mainCourse;

    int     mainRunWay;
    Wind    wind;

    ParamRw()
    {
        pressureLeft = -1;
        pressureRight = -1;
        mainRunWay = -1;
        mainCourse = -1;
        stateRunWay = -1;
    }

    bool isEmpty()
    {
        return pressureLeft < 0 && pressureRight < 0 && wind.isEmpty();
    }
};

struct ParamCommon
{
    QString atis;
    int transitioEchelon;
    QHash<QString,ParamRw> paramRw;                      //id, paramRw
    ParamCommon(/*const QStringList &names=QStringList()*/)
    {
        atis = "";
        transitioEchelon = -1;
       /* ParamRw rw;
        foreach (const QString & name, names)
            paramRw.insert(name,rw);*/
    }

    bool isEmpty()
    {
        return paramRw.isEmpty();   //rw
    }
};

#endif // STRUCT_METEO_H
