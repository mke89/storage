#include "SEvent.h"
#include <QCoreApplication>

StorageEventInfo::StorageEventInfo()
{
    operationCode = StorageOpCode::UNDEFINED;
    nanoSecsSinceEpoch = Utils::getNanoSecsSinceEpoch();
    setAppName(qApp->applicationName());
    setHostAddress(0);
    setShmKey("");
    dataCellIndex = -1;
    errorCode = StorageError::Code::EC_UNDEFINED;
    pid = QCoreApplication::applicationPid();
    payloadSize = 0;
}

DataKey StorageEventInfo::getDataKey() const
{
    return DataKey(shmKey, dataCellIndex, errorCode);
}

void StorageEventInfo::setDataKey(const DataKey &dataKey)
{
    Utils::copyFromQStringToCharArr(this->shmKey,
                                    SHM_KEY_LENGTH, dataKey.shmKey);
    dataCellIndex = dataKey.index;
    errorCode = dataKey.errorCode;
}

void StorageEventInfo::setAppName(const QString &appName)
{
    Utils::copyFromQStringToCharArr(this->appName,
                                    SOURCE_NAME_LENGTH, appName);
}

QString StorageEventInfo::getAppName() const
{
    return QString(appName);
}

void StorageEventInfo::setShmKey(const QString &shmKey)
{
    Utils::copyFromQStringToCharArr(this->shmKey,
                                    SHM_KEY_LENGTH, shmKey);
}

QString StorageEventInfo::getShmKey()
{
    return QString(shmKey);
}

QString StorageEventInfo::opCodeString() const
{
    return storageOpCodeToString(operationCode);
}

void StorageEventInfo::setHostAddress(const Utils::IPv4Address address)
{
    hostAddr[0] = address.byte1;
    hostAddr[1] = address.byte2;
    hostAddr[2] = address.byte3;
    hostAddr[3] = address.byte4;
}

void StorageEventInfo::setHostAddress(uint32_t address)
{
    uint8_t *addressPtr = (uint8_t*)&address;

    hostAddr[0] = addressPtr[3];
    hostAddr[1] = addressPtr[2];
    hostAddr[2] = addressPtr[1];
    hostAddr[3] = addressPtr[0];
}

QString storageOpCodeToString(StorageOpCode opCode)
{
    switch((int)opCode)
    {
        case (int)StorageOpCode::INSERT:
        {
            return QString("INSERT");
        }

        case (int)StorageOpCode::REMOVE:
        {
            return QString("REMOVE");
        }

        case (int)StorageOpCode::SELECT:
        {
            return QString("SELECT");
        }

        case (int)StorageOpCode::UPDATE:
        {
            return QString("UPDATE");
        }
    }

    return QString();
}

JournalEventInfo::JournalEventInfo():
    userName(""),
    messageType("")
{
    userId = -1;
    setUserName("System");
    setMessageType("Undefined");
}

JournalEventInfo::JournalEventInfo(const DataKey &dataKey,
                                   const UserIdentificator &userId,
                                   const QString &messageType,
                                   StorageOpCode opCode)
{
    setDataKey(dataKey);
    this->userId = userId.userId;
    setUserName(userId.userName);
    setMessageType(messageType);
    operationCode = opCode;
}

void JournalEventInfo::setUserName(const QString &userName)
{
    Utils::copyFromQStringToCharArr(this->userName, USER_NAME_LENGTH,
                                    userName);
}

QString JournalEventInfo::getUserName() const
{
    return QString(userName);
}

void JournalEventInfo::setMessageType(const QString &messageType)
{
    Utils::copyFromQStringToCharArr(this->messageType,
                                    MESSAGE_TYPE_LENGTH, messageType);
}

QString JournalEventInfo::getMessageType() const
{
    return QString(messageType);
}

QByteArray JournalEvent::toByteArray()
{
    int headerSize = sizeof(JournalEventInfo);
    int dataSize = data.size();
    int commonSize = headerSize + dataSize;
    char rawData[commonSize];

    memcpy(rawData, &eventInfo, headerSize);
    memcpy(rawData + headerSize, data.constData(), data.size());

    return QByteArray(rawData, commonSize);
}

void JournalEvent::setMessage(const QString &message)
{
    data = message.toLocal8Bit();
    eventInfo.payloadSize = message.size();
}


