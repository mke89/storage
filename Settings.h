#ifndef SETTINGS_H
#define SETTINGS_H

#include <QSettings>
#include <QDateTime>
#include <QString>
#include <QCoreApplication>

namespace SettingsParams
{
    const QString STORAGES_PREAMBLE     = "Storage.";
    const QString STORAGE_DESCRIPTION   = "description";
    const QString STORAGE_NAME          = "name";
    const QString STORAGE_SIZE          = "size";
    const QString STORAGE_ROW_SIZE      = "rowSize";
    const QString STORAGE_SEGMENT_ID    = "segmentId";
    const QString STORAGE_TYPE          = "type";
    const QString STORAGE_TYPE_FIXED    = "fixed";
    const QString STORAGE_TYPE_SCALABLE = "scalable";

    const QString NETWORK_GROUP         = "Network";
    const QString NOTIFICATION_PORT     = "NotificationPort";
    const QString JOURNAL_PORT          = "JournalPort";

    const int DEFAULT_NOTIFICATION_PORT = 55555;
    const int DEFAULT_JOURNAL_PORT      = 55556;
}

namespace StorageSettings
{
    class Settings
    {
        public:
            static Settings &instance(QString confFileName = qApp->applicationName());

            void createDefaultSettings();
            QVariant value(const QString &key) const;
            QVariant value(QString key, QVariant defaultValue) const;
            void setValue(const QString &key, const QVariant &value);
            bool contains(const QString &key) const;
            void clearToTests();
            void removeGroup(const QString &groupName);
            QList<QString> allKeys() const;
            QList<QString> baseGroups() const;
            QList<QString> groupKeys(const QString &group);

        private:
            Settings();
            Settings(QString confFileName);
            Settings(const Settings& root);
            Settings& operator=(const Settings&);

            void setDefaultValue(const QString &name, const QVariant &value);

            QSettings settings;
    };
}

#endif // SETTINGS_H
