#include "Utils.h"
#include <chrono>
#include <QHostAddress>
#include <QNetworkInterface>

Utils::Utils()
{

}

Utils::IPv4Address::IPv4Address():
    byte1(0),
    byte2(0),
    byte3(0),
    byte4(0)
{}

Utils::IPv4Address::IPv4Address(uchar b1, uchar b2, uchar b3, uchar b4):
    byte1(b1),
    byte2(b2),
    byte3(b3),
    byte4(b4)
{}

Utils::IPv4Address::IPv4Address(const uint8_t *address):
    byte1(address[0]),
    byte2(address[1]),
    byte3(address[2]),
    byte4(address[3])
{

}

bool Utils::IPv4Address::equal(const IPv4Address &addr) const
{
    if((addr.byte1 != byte1) || (addr.byte2 != byte2) ||
       (addr.byte3 != byte3) || (addr.byte4 != byte4))
    {
        return false;
    }

    return true;
}

QString Utils::IPv4Address::toString() const
{
    return QString("%1.%2.%3.%4").arg(byte1).arg(byte2).arg(byte3).arg(byte4);
}

uint32_t Utils::IPv4Address::bytesToUInt(const uint8_t *address)
{
    if(!address)
    {
        return 0;
    }

    uint32_t ip = address[0];

    ip <<= 8;
    ip |= address[1];

    ip <<= 8;
    ip |= address[2];

    ip <<= 8;
    ip |= address[3];

    return ip;
}

quint64 Utils::getNanoSecsSinceEpoch()
{
    return std::chrono::time_point_cast<std::chrono::nanoseconds>
            (std::chrono::system_clock::now()).time_since_epoch().count();
}

quint64 Utils::getMicroSecsSinceEpoch()
{
    return std::chrono::time_point_cast<std::chrono::microseconds>
            (std::chrono::system_clock::now()).time_since_epoch().count();
}

quint64 Utils::getMilliSecsSinceEpoch()
{
    return std::chrono::time_point_cast<std::chrono::milliseconds>
            (std::chrono::system_clock::now()).time_since_epoch().count();
}

QList<uint32_t> Utils::getCurrentIPv4AllAddresses()
{
    QList<quint32> ipv4Addrs;
    const QList<QHostAddress> &addrList = QNetworkInterface::allAddresses();
    for(const QHostAddress &addr: addrList)
    {
        ipv4Addrs << addr.toIPv4Address();
    }

    return ipv4Addrs;
}

Utils::IPv4Address Utils::IPv4AddressFromQUint32(quint32 address)
{
    uint8_t *addrPtr = (uint8_t*)&address;
    return Utils::IPv4Address(addrPtr[3], addrPtr[2], addrPtr[1], addrPtr[0]);
}

bool Utils::copyFromQStringToCharArr(char *arr, int arrSize, const QString &string)
{
    if(!arr)
    {
        return false;
    }
    int stringLength = string.size();
    if(stringLength >= arrSize)
    {
        stringLength = arrSize - 1;
    }

    const QByteArray &stringData = string.toLatin1();
    memcpy(arr, stringData.constData(), stringLength);
    arr[stringLength] = '\0';

    return true;
}

